% Project l1/linf (robust)
close all; clear all; clc;

%% Set up parameters

load IEEE14bus_DataSets.mat
load l1_lr_Valid.mat

tmp=1:nSets;
nSet=tmp(lr_median_error_V(1,:)~=0);
nTest=0.25*nData;

lrNom=lr_mean_error_V(1,:);
lambdaNom=lr_mean_error_V(2,:); 
lrRob=lr_mean_error_rob_V(1,:);
lambdaRob=lr_mean_error_rob_V(2,:);
rhoRob=lr_mean_error_rob_V(3,:);

median_error_T = zeros(nSets,1);   % 1: l1, 2: l2, 3: linf
mean_error_T = zeros(nSets,1);
std_error_T = zeros(nSets,1);
max_error_T = zeros(nSets,1);
min_error_T = zeros(nSets,1);
p25_error_T = zeros(nSets,1);
p75_error_T = zeros(nSets,1);

median_error_rob_T = zeros(nSets,1);   % 1: l1, 2: l2, 3: linf
mean_error_rob_T = zeros(nSets,1);
std_error_rob_T = zeros(nSets,1);
max_error_rob_T = zeros(nSets,1);
min_error_rob_T = zeros(nSets,1);
p25_error_rob_T = zeros(nSets,1);
p75_error_rob_T = zeros(nSets,1);


O=[];
for j=0:T-1
    O=[O;C*A^j];
end

J=zeros(p*T,m*T);
for i=1:T
    for j=1:i
        if i==j
            J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D;
        else
            for k=1:T-1
                if i-j==k
                    J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C*A^(k-1)*B;
                end
            end
        end
    end
end

tStart = tic;

for kkp=nSet%1:nSet

        %% Nominal
        lambda=lambdaNom(kkp);
        lr=lrNom(kkp);
        if lr==3
            lr=Inf;
        end
        e_nom=[];
        %     display(kka)
        
        for kkk=1:nTest
            %         display(kkk)
            Y=YMat_TestSet(:,:,kkk,kkp);
            u=UMat_TestSet(:,:,kkk,kkp);
            x0=XMat_TestSet(:,:,kkk,kkp);
            
            Yvec=vec(Y);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            if lr==1
                cvx_solver sdpt3 %Gurobi
                cvx_precision medium
                cvx_begin quiet
                
                variable xhatnom( n )
                variable dhatnom( m*T)
                
                ehat=Yvec-O*xhatnom-J*(vec(u)+vec(dhatnom));
                obj=norm(ehat,1)+lambda*norm(dhatnom,1);
                minimize( obj ) % Unconstrained minimization
                
                cvx_end
                cvx_clear
            else
                cvx_solver sdpt3 %Gurobi
                cvx_precision medium
                cvx_begin quiet
                
                variable xhatnom( n )
                variable Dhatnom( m , T)
                Phi=[];
                Theta_D=[];
                for i=1:T
                    Phi=[Phi C*A^(i-1)*xhatnom];
                    sum_tmp=zeros(n,1);
                    for j=0:i-2
                        tmp2=i-2-j;
                        if tmp2>=0
                            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1));
                        end
                    end
                    Theta_D=[Theta_D D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
                end
                Ehat=Y-Phi-Theta_D;
                obj=0;%rho*norm(xhatnom,1);
                for i=1:p
                    obj=obj+norm(Ehat(i,:),lr);
                end
                for i=1:m
                    obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
                end
                % obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
                minimize( obj ) % Unconstrained minimization
                
                cvx_end
                cvx_clear
            end
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom=[e_nom; error_nom];
            
        end
        median_error_T(kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error_T(kkp) = mean(e_nom);
        std_error_T(kkp) = std(e_nom);
        max_error_T(kkp) = max(e_nom);
        min_error_T(kkp) = min(e_nom);
        p25_error_T(kkp) = prctile(e_nom,25);
        p75_error_T(kkp) = prctile(e_nom,75);
        
        
        %% Robust
        lambda=lambdaRob(kkp);
        rho=rhoRob(kkp);
        lr=lrRob(kkp);
        if lr==3
            lr=Inf;
        end
        e_rob=[];
        %     display(kka)
        
        for kkk=1:nTest
            %         display(kkk)
            Y=YMat_TestSet(:,:,kkk,kkp);
            u=UMat_TestSet(:,:,kkk,kkp);
            x0=XMat_TestSet(:,:,kkk,kkp);
            
            Yvec=vec(Y);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            if lr==1
                cvx_solver sdpt3 %Gurobi
                cvx_precision medium
                cvx_begin quiet
                
                variable xhatrob( n )
                variable dhatrob( m , T)
                ehatrob=Yvec-O*xhatrob-J*(vec(u)+vec(dhatrob));
                obj=norm(ehatrob,1)+rho*norm(xhatrob,1)+(rho+lambda)*norm(dhatrob,1);
                minimize( obj ) % Unconstrained minimization
                
                cvx_end
                cvx_clear
                
            else
                cvx_solver sdpt3 %Gurobi
                cvx_precision medium
                cvx_begin quiet
                
                variable xhatrob( n )
                variable Dhatrob( m , T)
                Phi=[];
                Theta_D=[];
                for i=1:T
                    Phi=[Phi C*A^(i-1)*xhatrob];
                    sum_tmp=zeros(n,1);
                    for j=0:i-2
                        tmp2=i-2-j;
                        if tmp2>=0
                            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatrob(:,j+1));
                        end
                    end
                    Theta_D=[Theta_D D*(u(:,i)+Dhatrob(:,i))+C*sum_tmp];
                end
                Ehatrob=Y-Phi-Theta_D;
                obj=rho*norm(xhatrob,1);
                for i=1:p
                    obj=obj+norm(Ehatrob(i,:),lr);
                end
                for i=1:m
                    obj=obj+rho*norm(Dhatrob(i,:),1)+lambda*norm(Dhatrob(i,:),lr);
                end
                minimize( obj ) % Unconstrained minimization
                
                cvx_end
                cvx_clear
               
            end
            error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
            e_rob=[e_rob; error_rob];
            
        end
        
        median_error_rob_T(kkp) = median(e_rob);   % (qactuators,qsensors)
        mean_error_rob_T(kkp) = mean(e_rob);
        std_error_rob_T(kkp) = std(e_rob);
        max_error_rob_T(kkp) = max(e_rob);
        min_error_rob_T(kkp) = min(e_rob);
        p25_error_rob_T(kkp) = prctile(e_rob,25);
        p75_error_rob_T(kkp) = prctile(e_rob,75);
        
        
end

avg_median_error_T = mean(median_error_T(1:nSet));  
avg_mean_error_T = mean(mean_error_T(1:nSet));
avg_std_error_T = mean(std_error_T(1:nSet));
avg_max_error_T = mean(max_error_T(1:nSet));
avg_min_error_T = mean(min_error_T(1:nSet));
avg_p25_error_T = mean(p25_error_T(1:nSet));
avg_p75_error_T = mean(p75_error_T(1:nSet));

avg_median_error_rob_T = mean(median_error_rob_T(1:nSet));
avg_mean_error_rob_T = mean(mean_error_rob_T(1:nSet));
avg_std_error_rob_T = mean(std_error_rob_T(1:nSet));
avg_max_error_rob_T = mean(max_error_rob_T(1:nSet));
avg_min_error_rob_T = mean(min_error_rob_T(1:nSet));
avg_p25_error_rob_T = mean(p25_error_rob_T(1:nSet));
avg_p75_error_rob_T = mean(p75_error_rob_T(1:nSet));




tElapsed = toc(tStart);

save('l1_lr_Test_median.mat','tElapsed','avg_median_error_T','avg_mean_error_T','avg_std_error_T','avg_max_error_T','avg_min_error_T','avg_p25_error_T','avg_p75_error_T','avg_median_error_rob_T','avg_mean_error_rob_T','avg_std_error_rob_T','avg_max_error_rob_T','avg_min_error_rob_T','avg_p25_error_rob_T','avg_p75_error_rob_T', ...
    'median_error_T','mean_error_T','std_error_T','max_error_T','min_error_T','p25_error_T','p75_error_T','median_error_rob_T','mean_error_rob_T','std_error_rob_T','max_error_rob_T','min_error_rob_T','p25_error_rob_T','p75_error_rob_T')
% save('l1_lr_Test.mat','tElapsed','avg_median_error_T','avg_mean_error_T','avg_median_error_rob_T','avg_mean_error_rob_T', ...
%     'median_error_T','mean_error_T','median_error_rob_T','mean_error_rob_T')
display('Attack+Uncertainty')
