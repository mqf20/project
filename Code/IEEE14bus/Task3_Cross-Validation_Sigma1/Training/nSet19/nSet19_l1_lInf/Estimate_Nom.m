
function xhatnom=Estimate_Nom(Y,u,A,B,C,D,n,m,p,T,lr,lambda)

cvx_solver sdpt3 %Gurobi
cvx_precision medium
cvx_begin quiet

variable xhatnom( n )
variable Dhatnom( m , T)
Phi=[];
Theta_D=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhatnom];
    sum_tmp=zeros(n,1);
    for j=0:i-2
        tmp2=i-2-j;
        if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1));
        end
    end
    Theta_D=[Theta_D D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
end
Ehat=Y-Phi-Theta_D;
obj=0;%rho*norm(xhatnom,1);
for i=1:p
    obj=obj+norm(Ehat(i,:),lr);
end
for i=1:m
    obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
end
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
minimize( obj ) % Unconstrained minimization

cvx_end
cvx_clear

end