close all; clear all; clc;

%% Set up parameters

max_iter = 3; % Number of iterations

load Atilde.mat
load Ctilde.mat

Ts = 0.05; % Discretization time step
n = size(Atilde,1);
Btilde = [zeros(n/2,n/2); eye(n/2,n/2)]; m = size(Btilde,2);
C = Ctilde; p = size(C,1);
Mexp=expm([Atilde Btilde; zeros(m,n+m)]*Ts);
A=Mexp(1:n,1:n);
B=Mexp(1:n,n+1:end);
D = zeros(p,m);

rho1 = 0.1;
rho2 = 1;
lambda =0.2;%0.15; % For l2, lambda=[5,15] (10), % For l_inf, lambda=[0.05,1] (0.5)
lr=inf; % 2 and inf, crashes occasionally when I use lr=1

T = 1.5*n; %15; % Number of time-steps
max_qa = 5; % Maximum number of actuators attacked
max_qs = 10; % Maximum number of sensors attacked
qavec = 0:max_qa;
qsvec = 0:max_qs;
numqa = length(qavec); % Number of attacked actuators
numqs = length(qsvec); % Number of attacked sensors

median_error = zeros(numqa,numqs);   % (qactuators,qsensors)
mean_error = zeros(numqa,numqs);
std_error = zeros(numqa,numqs);
max_error = zeros(numqa,numqs);
min_error = zeros(numqa,numqs);
p25_error = zeros(numqa,numqs);
p75_error = zeros(numqa,numqs);

median_error_r1 = zeros(numqa,numqs);   % (qactuators,qsensors)
mean_error_r1 = zeros(numqa,numqs);
std_error_r1 = zeros(numqa,numqs);
max_error_r1 = zeros(numqa,numqs);
min_error_r1 = zeros(numqa,numqs);
p25_error_r1 = zeros(numqa,numqs);
p75_error_r1 = zeros(numqa,numqs);

median_error_r2 = zeros(numqa,numqs);   % (qactuators,qsensors)
mean_error_r2 = zeros(numqa,numqs);
std_error_r2 = zeros(numqa,numqs);
max_error_r2 = zeros(numqa,numqs);
min_error_r2 = zeros(numqa,numqs);
p25_error_r2 = zeros(numqa,numqs);
p75_error_r2 = zeros(numqa,numqs);

tStart = tic;

for kka=1:numqa,
    for kks=1:numqs,
      
        q_a = qavec(kka); % Number of actuators attacked
        q_s = qsvec(kks); % Number of sensors attacked
        
        e_rob=[];
        e_rob2=[];
        e_nom=[];
        iter=1;
        
        while iter <=  max_iter %

            x0=randn(n,1);
            
            rm=randperm(m);
            ra=rm(1:m-q_a); % Actuators that are NOT attacked
            G=eye(m); 
            G(:,ra)=[]; % Actuator attack matrix
            
            rp=randperm(p-1);
            rs=rp(1:p-1-q_s); % Sensors that are NOT attacked
            H=eye(p);
            H(:,rs)=[]; % Sensor attack matrix
            H(:,end)=[]; % the last measurement (the rotor angle) cannot be attacked
                                    
            dA = [zeros(n/2,n); 0.01*randn(n/2,n/2), zeros(n/2,n/2)];
            dB = [zeros(n/2,m);diag(0.1*randn(m,1))];
            dC = [0.1*randn(p,n/2), zeros(p,n/2)]; dD = 0;
            Mexp_tilde=expm([Atilde+dA Btilde+dB; zeros(m,n+m)]*Ts);
            A_tilde=Mexp(1:n,1:n);
            B_tilde=Mexp(1:n,n+1:end);
            C_tilde=C+dC; D_tilde =D+dD;
            
            display(iter)
            if ~(all(abs(eig(A))<=1+1e-10) && all(abs(eig(A_tilde))<1+1e-10)) % If unstable
                
                display('reject')
                continue
            else
                iter=iter+1;
            end
            
            Q=[eps*eye(n/2),zeros(n/2,n/2);zeros(n/2,n/2), 1e-1*eye(n/2)];%diag([0.1 0.16 0.2 0.12 0.25 0.14 0.03 0.21 0.3 0.02 0.09 0.18]);
            R=1e-1*eye(p);%*diag([0.21 0.06 0.22 0.02 0.19 0.14 0.13 0.11 0.23 0.12 0.03 0.18]);
            
            %% Attacks and noises
            
            % Generate attacks
            attackpower = 1;
            d=attackpower*randn(q_a,T);
            e=attackpower*randn(q_s,T);
            u=0.01*randn(m,T);
            
            % Generate noises
            SigmaQ = chol(Q);
            w = (randn(T,n)*SigmaQ)';
            SigmaR = chol(R);
            v = (randn(T,p)*SigmaR)';
            
            %% Actual dynamics
            
            X=zeros(n,T);
            Y=zeros(p,T);
            X(:,1)=x0;
            for i=1:T
                Y(:,i)=C_tilde*X(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
                if i<T
                    X(:,i+1)=A_tilde*X(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
                end
                %     % no uncertainties
                %     y(:,i)=C*x(:,i)+D*G*d(:,i)+H*e(:,i);
                %     if i<T
                %         x(:,i+1)=A*x(:,i)+B*G*d(:,i);
                %     end
            end
                        
            %% l1/lr robust estimator with rho1
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
                variable xhat( n )
                variable Dhat( m , T)
                Phi=[];
                Theta_D=[];
                for i=1:T
                    Phi=[Phi C*A^(i-1)*xhat];
                    sum_tmp=zeros(n,1);
                    for j=0:i-2
                        tmp2=i-2-j;
                        if tmp2>=0
                            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat(:,j+1));
                        end
                    end
                    Theta_D=[Theta_D D*(u(:,i)+Dhat(:,i))+C*sum_tmp];
                end
                Ehat=Y-Phi-Theta_D;
                obj=rho1*norm(xhat,1);
                for i=1:p
                    obj=obj+norm(Ehat(i,:),lr);
                end
                for i=1:m
                    obj=obj+rho1*norm(Dhat(i,:),1)+lambda*norm(Dhat(i,:),lr);
                end
                minimize( obj ) % Unconstrained minimization
                
                cvx_end            
                cvx_clear    

            
            %% l1/lr robust estimator with rho2
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
                variable xhat2( n )
                variable Dhat2( m , T)
                Phi=[];
                Theta_D=[];
                for i=1:T
                    Phi=[Phi C*A^(i-1)*xhat2];
                    sum_tmp=zeros(n,1);
                    for j=0:i-2
                        tmp2=i-2-j;
                        if tmp2>=0
                            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat2(:,j+1));
                        end
                    end
                    Theta_D=[Theta_D D*(u(:,i)+Dhat2(:,i))+C*sum_tmp];
                end
                Ehat2=Y-Phi-Theta_D;
                obj=rho2*norm(xhat2,1);
                for i=1:p
                    obj=obj+norm(Ehat2(i,:),lr);
                end
                for i=1:m
                    obj=obj+rho2*norm(Dhat2(i,:),1)+lambda*norm(Dhat2(i,:),lr);
                end
                minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            %% Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
                variable xhatnom( n )
                variable Dhatnom( m , T)
                Phi=[];
                Theta_D=[];
                for i=1:T
                    Phi=[Phi C*A^(i-1)*xhatnom];
                    sum_tmp=zeros(n,1);
                    for j=0:i-2
                        tmp2=i-2-j;
                        if tmp2>=0
                            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1));
                        end
                    end
                    Theta_D=[Theta_D D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
                end
                Ehat=Y-Phi-Theta_D;
                obj=0;%rho*norm(xhatnom,1);
                for i=1:p
                    obj=obj+norm(Ehat(i,:),lr);
                end
                for i=1:m
                    obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
                end
                % obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
                minimize( obj ) % Unconstrained minimization
            
            cvx_end            
            cvx_clear
            
  
            error=norm((xhat-x0))/(norm(x0)+eps);
            error2=norm((xhat2-x0))/(norm(x0)+eps);
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_rob=[e_rob; error];
            e_rob2=[e_rob2; error2];
            e_nom=[e_nom; error_nom];

        end
        
        median_error(kka,kks) = median(e_nom);   % (qactuators,qsensors)
        mean_error(kka,kks) = mean(e_nom);
        std_error(kka,kks) = std(e_nom);
        max_error(kka,kks) = max(e_nom);
        min_error(kka,kks) = min(e_nom);
        p25_error(kka,kks) = prctile(e_nom,25);
        p75_error(kka,kks) = prctile(e_nom,75);
        
        median_error_r1(kka,kks) = median(e_rob);   % (qactuators,qsensors)
        mean_error_r1(kka,kks) = mean(e_rob);
        std_error_r1(kka,kks) = std(e_rob);
        max_error_r1(kka,kks) = max(e_rob);
        min_error_r1(kka,kks) = min(e_rob);
        p25_error_r1(kka,kks) = prctile(e_rob,25);
        p75_error_r1(kka,kks) = prctile(e_rob,75);
        
        median_error_r2(kka,kks) = median(e_rob2);   % (qactuators,qsensors)
        mean_error_r2(kka,kks) = mean(e_rob2);
        std_error_r2(kka,kks) = std(e_rob2);
        max_error_r2(kka,kks) = max(e_rob2);
        min_error_r2(kka,kks) = min(e_rob2);
        p25_error_r2(kka,kks) = prctile(e_rob2,25);
        p75_error_r2(kka,kks) = prctile(e_rob2,75);
%         keyboard
        
    end
end

tElapsed = toc(tStart);

% avg=avg/Iter
% avg_nom=avg_nom/Iter
% boxplot([e_rob,e_rob2,e_nom],'whisker',1000)
% display(count)

save('l1_lr_Array.mat','tElapsed','qavec','qsvec','rho1','rho2','lambda','lr','median_error','median_error_r1','median_error_r2','mean_error','mean_error_r1','mean_error_r2','std_error','std_error_r1','std_error_r2','min_error','min_error_r1','min_error_r2','max_error','max_error_r1','max_error_r2','p25_error','p25_error_r1','p25_error_r2','p75_error','p75_error_r1','p75_error_r2')
display('Attack+Uncertainty')

%%

% figure
% minc=min([min(min(mean_error)),min(min(mean_error_r1)),min(min(mean_error_r2))]);
% maxc=max([max(max(mean_error)),max(max(mean_error_r1)),max(max(mean_error_r2))]);
% subplot(2,3,1)
% imagesc(qsvec,qavec,mean_error);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Success rate of l1 decoder as function ', ...
%     'of number of attacked sensors and actuators', ...
%     'IEEE 14-bus power network example'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])
% 
% %figure;
% subplot(2,3,2)
% imagesc(qsvec,qavec,mean_error_r1);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Success rate of l1 decoder as function ', ...
%     'of number of attacked sensors and actuators', ...
%     'IEEE 14-bus power network example'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])
% 
% 
% %figure;
% subplot(2,3,3)
% imagesc(qsvec,qavec,mean_error_r2);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Success rate of l1 decoder as function ', ...
%     'of number of attacked sensors and actuators', ...
%     'IEEE 14-bus power network example'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])
% 
% 
% minc=min([min(min(median_error)),min(min(median_error_r1)),min(min(median_error_r2))]);
% maxc=max([max(max(median_error)),max(max(median_error_r1)),max(max(median_error_r2))]);
% subplot(2,3,4)
% imagesc(qsvec,qavec,median_error);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Success rate of l1 decoder as function ', ...
%     'of number of attacked sensors and actuators', ...
%     'IEEE 14-bus power network example'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])
% 
% %figure;
% subplot(2,3,5)
% imagesc(qsvec,qavec,median_error_r1);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Success rate of l1 decoder as function ', ...
%     'of number of attacked sensors and actuators', ...
%     'IEEE 14-bus power network example'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])
% 
% %figure;
% subplot(2,3,6)
% imagesc(qsvec,qavec,median_error_r2);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Success rate of l1 decoder as function ', ...
%     'of number of attacked sensors and actuators', ...
%     'IEEE 14-bus power network example'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])

