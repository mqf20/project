% Project l1/l2

close all; clear all; clc;
Iter=50;
count=0;
avg=0;
avg_nom=0;

load Atilde.mat
load Ctilde.mat

Ts = 0.05;
n = size(Atilde,1);
Btilde = [zeros(n/2,n/2); eye(n/2,n/2)]; m = size(Btilde,2);
C = Ctilde; p = size(C,1);
Mexp=expm([Atilde Btilde; zeros(m,n+m)]*Ts);
A=Mexp(1:n,1:n);
B=Mexp(1:n,n+1:end);
D = zeros(p,m);

rho1 = 0.025;
rho2 = 0.25;
lambda =1e-3;%0.15; % For l2, lambda=[5,15] (10), % For l_inf, lambda=[0.05,1] (0.5)
lr=inf; % 2 and inf, crashes occasionally when I use lr=1

% n = 20;
% m = 5;
% p = 20; %15
T = 1.5*n;%15; % Number of time-steps
q_s = 1;%5;
q_a = 3;%1;
% max_qa = 5;
% max_qs = 10;

e_rob=[];
e_rob2=[];
e_nom=[];

iter=1;

% while iter <=  Iter %
for iter=1:Iter
    
x0=randn(n,1);

rm=randperm(m);
ra=rm(1:m-q_a);
G=eye(m);
G(:,ra)=[];

rp=randperm(p-1);
rs=rp(1:p-1-q_s);
H=eye(p);
H(:,rs)=[];
H(:,end)=[]; % the last measurement (the rotor angle) cannot be attacked

Gt=B*[G zeros(m,1)];
Ht=[zeros(p,3) H];
tzero(A,Gt,C,Ht)

end




