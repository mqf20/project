% Project l1/l2

clear all; clc; close all; 

display('Uncertainty Only')
% load l1_linf_traj_q13_attack.mat

l2_1=load('l1_linf_traj_q13_both.mat');
l2_attack=load('l1_linf_traj_q13_attack.mat');
l2_error=load('l1_linf_traj_q13_error.mat');

max_error_nom = max(l2_1.e_nom);
p75_error_nom = prctile(l2_1.e_nom,75);

nom_A = l2_attack.e_nom;
rob1_A = l2_attack.e_rob;
rob2_A = l2_attack.e_rob2;

nom_B = l2_error.e_nom;
rob1_B = l2_error.e_rob;
rob2_B = l2_error.e_rob2;

nom_C = l2_1.e_nom;
rob1_C = l2_1.e_rob;
rob2_C = l2_1.e_rob2;

set(0,'defaulttextinterpreter','latex')
f=figure;   
% Boxplot for the observed temperature from January to December 
Temp_nom = [nom_A(:,1), nom_B(:,1), nom_C(:,1)];
position_O = 1:1.4:5;  
% Define position for 12 Month_O boxplots  
box_O = boxplot(Temp_nom,'colors','b','positions',position_O,'width',0.18,'whisker',1e10);   
hold on  % Keep the Month_O boxplots on figure overlap the Month_S boxplots 

% Boxplot for the simulated temperature from January to December 
Temp_rob1 = [rob1_A(:,1), rob1_B(:,1), rob1_C(:,1)];%[Jan_S, Feb_S, Mar_S, Apr_S, May_S, Jun_S];%, Jul_S, Aug_S, Sep_S, Oct_S, Nov_S, Dec_S]; 
position_S = 1.4:1.4:5.3;  % Define position for 12 Month_S boxplots  
box_S = boxplot(Temp_rob1,'colors','r','positions',position_S,'width',0.18,'whisker',1e10);   
set(gca,'XTickLabel',{' '})  % Erase xlabels

Temp_rob2 = [rob2_A(:,1), rob2_B(:,1), rob2_C(:,1)];%[Jan_S, Feb_S, Mar_S, Apr_S, May_S, Jun_S];%, Jul_S, Aug_S, Sep_S, Oct_S, Nov_S, Dec_S]; 
position_3 = 1.8:1.4:5.6;  % Define position for 12 Month_S boxplots  
box_3 = boxplot(Temp_rob2,'colors',[0 0.5 0],'positions',position_3,'width',0.18,'whisker',1e10); 
set(gca,'XTickLabel',{' '})  % Erase xlabels
hold off   % Insert texts and labels 
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold') 
text('Position',[0.85,-0.25],'String','Nom.','FontSize',12,'fontWeight','bold') 
text('Position',[1.3,-0.25],'String','Rob.','FontSize',12,'fontWeight','bold')
text('Position',[1.7,-0.25],'String','Rob.','FontSize',12,'fontWeight','bold')
text('Position',[2.3,-0.25],'String','Nom.','FontSize',12,'fontWeight','bold') 
text('Position',[2.7,-0.25],'String','Rob.','FontSize',12,'fontWeight','bold') 
text('Position',[3.1,-0.25],'String','Rob.','FontSize',12,'fontWeight','bold')
text('Position',[3.7,-0.25],'String','Nom.','FontSize',12,'fontWeight','bold') 
text('Position',[4.1,-0.25],'String','Rob.','FontSize',12,'fontWeight','bold') 
text('Position',[4.5,-0.25],'String','Rob.','FontSize',12,'fontWeight','bold')

text('Position',[1.1,-0.6],'String','Attack only','FontSize',14,'fontWeight','bold') 
text('Position',[2.3,-0.6],'String','Uncertainty only','FontSize',14,'fontWeight','bold')
text('Position',[3.6,-0.6],'String','Uncertainty and attack','FontSize',14,'fontWeight','bold') 

text('Position',[0.85,-0.42],'String','$\rho=0$','FontSize',10,'fontWeight','bold')
text('Position',[1.2,-0.42],'String','$\rho=0.1$','FontSize',10,'fontWeight','bold')
text('Position',[1.7,-0.42],'String','$\rho=1.0$','FontSize',10,'fontWeight','bold')
text('Position',[2.3,-0.42],'String','$\rho=0$','FontSize',10,'fontWeight','bold')
text('Position',[2.6,-0.42],'String','$\rho=0.1$','FontSize',10,'fontWeight','bold')
text('Position',[3.05,-0.42],'String','$\rho=1.0$','FontSize',10,'fontWeight','bold')
text('Position',[3.675,-0.42],'String','$\rho=0$','FontSize',10,'fontWeight','bold')
text('Position',[4.0,-0.42],'String','$\rho=0.1$','FontSize',10,'fontWeight','bold')
text('Position',[4.45,-0.42],'String','$\rho=1.0$','FontSize',10,'fontWeight','bold')

xlim([0.6 5])
ylim([-0.05 ceil(max_error_nom(1))])
% breakyaxis([p75_error_nom(1)+0.25 ceil(max_error_nom(1))-0.5]);
set(gca,'FontSize',12) %,'fontWeight','bold')

% set(gca,'XTickLabel',{''});   % To hide outliers 
% out_O = box_O(end,~isnan(box_O(end,:)));  
% delete(out_O)  
% out_S = box_S(end,~isnan(box_S(end,:)));  
% delete(out_S)
title('$\ell_1/\ell_{\infty}$ Relaxation')

% median_error_nom = median(e_nom);   
% mean_error_nom = mean(e_nom);
% std_error_nom = std(e_nom);
% max_error_nom = max(e_nom);
% min_error_nom = min(e_nom);
% p25_error_nom = prctile(e_nom,25);
% p75_error_nom = prctile(e_nom,75);
% 
% median_error_rob = median(e_rob);   
% mean_error_rob = mean(e_rob);
% std_error_rob = std(e_rob);
% max_error_rob = max(e_rob);
% min_error_rob = min(e_rob);
% p25_error_rob = prctile(e_rob,25);
% p75_error_rob = prctile(e_rob,75);
% 
% median_error_rob2 = median(e_rob2);   
% mean_error_rob2 = mean(e_rob2);
% std_error_rob2 = std(e_rob2);
% max_error_rob2 = max(e_rob2);
% min_error_rob2 = min(e_rob2);
% p25_error_rob2 = prctile(e_rob2,25);
% p75_error_rob2 = prctile(e_rob2,75);
% 
% figure
% boxplot([e_rob(:,1),e_rob2(:,1),e_nom(:,1)],'whisker',1000)
% breakyaxis([p75_error_nom(1)+1 ceil(max_error_nom(1))-1]);
% 
% figure
% subplot(4,1,1)
% errorbar(k,median_error_nom,p25_error_nom,p75_error_nom,'k-')
% hold all;
% errorbar(k,median_error_rob,p25_error_rob,p75_error_rob,'b--')
% errorbar(k,median_error_rob2,p25_error_rob2,p75_error_rob2,'r-.')
% ylabel('median with p25 and p75')
% subplot(4,1,2)
% errorbar(k,median_error_nom,min_error_nom,max_error_nom,'k-')
% hold all;
% errorbar(k,median_error_rob,min_error_rob,max_error_rob,'b--')
% errorbar(k,median_error_rob2,min_error_rob2,max_error_rob2,'r-.')
% ylabel('median with min and max')
% subplot(4,1,3)
% errorbar(k,mean_error_nom,std_error_nom,'k-')
% hold all;
% errorbar(k,mean_error_rob,std_error_rob,'b--')
% errorbar(k,mean_error_rob2,std_error_rob2,'r-.')
% ylabel('mean with std')
% subplot(4,1,4)
% errorbar(k,mean_error_nom,min_error_nom,max_error_nom,'k-')
% hold all;
% errorbar(k,mean_error_rob,min_error_rob,max_error_rob,'b--')
% errorbar(k,mean_error_rob2,min_error_rob2,max_error_rob2,'r-.')
% ylabel('mean with min and max')

