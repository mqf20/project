function combine_results()

    % This simple function combines the results of results of
    % 'IEEE_14_vary_sigma_results_part1.mat' and 
    % 'IEEE_14_vary_sigma_results_part2.mat'
    %
    % The combined results are saved in the file 
    % 'IEEE_14_vary_sigma_results_part.mat' and the results contain the
    % following:
    % 
    % median_error - median relative (L2 norm) error for each value of 
    %                sigma
    % mean_error - mean of the relative error...
    % std_error - standard deviation of the relative error...
    % min_error - minimum of the relative error...
    % max_error - maximum of the relative error...
    % p25_error - 25th percentile of the relative reror...
    % p75_error - 75th percentile of the relative reror...
    % 
    % The following constants are also included:
    % 
    % n = 10;
    % p = 35;
    % m = 5;
    % sigmaVec = 0:0.1:10;
    % rho      = 0.025;
    % lambda   = 1e-3;
    % T        = 1.5 * n; %15; % Number of time-steps
    % numqa    = 1;       % Number of attacked actuators
    % numqs    = 3;       % Number of attacked sensors

    clear all;

    n = 10;
    p = 35;
    m = 5;
    sigmaVec = 0:0.1:5;
    rho1     = 0.1;
    rho2     = 1;
    lambda   = 0.2;
    T        = 1.5 * n; %15; % Number of time-steps
    numqa    = 1;       % Number of attacked actuators
    numqs    = 3;       % Number of attacked sensors

    load('IEEE_14_vary_sigma_results_part2a.mat','median_error','mean_error',...
        'std_error','min_error','max_error','p25_error','p75_error');

    % Temporary holder
    median_error_part2a = median_error;
    mean_error_part2a = mean_error;
    std_error_part2a = std_error;
    min_error_part2a = min_error;
    max_error_part2a = max_error;
    p25_error_part2a = p25_error;
    p75_error_part2a = p75_error;
    
    clear median_error mean_error std_error min_error max_error p25_error p75_error
    
    load('IEEE_14_vary_sigma_results_part2b.mat','median_error','mean_error',...
        'std_error','min_error','max_error','p25_error','p75_error');
      
    % Temporary holder
    median_error_part2b = median_error;
    mean_error_part2b = mean_error;
    std_error_part2b = std_error;
    min_error_part2b = min_error;
    max_error_part2b = max_error;
    p25_error_part2b = p25_error;
    p75_error_part2b = p75_error;

    clear median_error mean_error std_error min_error max_error p25_error p75_error

    load('IEEE_14_vary_sigma_results_part1.mat','median_error','mean_error',...
        'std_error','min_error','max_error','p25_error','p75_error');
      
    % Combine results
    fields1 = fieldnames(median_error); % 'l1_l1', 'l1_l2', 'l1_linf', 'nom_l1',  'nom_l2', 'nom_linf'
    fields2 = fieldnames(median_error.l1_l1); % 'rho1' and 'rho2'
    
    % Robust estimators
    for iter1 = 1 : 3
        for iter2 = 1 : numel(fields2)
            median_error.(fields1{iter1}).(fields2{iter2}) = [median_error.(fields1{iter1}).(fields2{iter2}); ...
                median_error_part2a.(fields1{iter1}).(fields2{iter2}); median_error_part2b.(fields1{iter1}).(fields2{iter2})];
            mean_error.(fields1{iter1}).(fields2{iter2})   = [mean_error.(fields1{iter1}).(fields2{iter2}); ...
                mean_error_part2a.(fields1{iter1}).(fields2{iter2}); mean_error_part2b.(fields1{iter1}).(fields2{iter2})];
            std_error.(fields1{iter1}).(fields2{iter2})    = [std_error.(fields1{iter1}).(fields2{iter2}); ...
                std_error_part2a.(fields1{iter1}).(fields2{iter2}); std_error_part2b.(fields1{iter1}).(fields2{iter2})];
            min_error.(fields1{iter1}).(fields2{iter2}) = [min_error.(fields1{iter1}).(fields2{iter2}); ...
                min_error_part2a.(fields1{iter1}).(fields2{iter2}); min_error_part2b.(fields1{iter1}).(fields2{iter2})];
            max_error.(fields1{iter1}).(fields2{iter2}) = [max_error.(fields1{iter1}).(fields2{iter2}); ...
                max_error_part2a.(fields1{iter1}).(fields2{iter2}); max_error_part2b.(fields1{iter1}).(fields2{iter2})];
            p25_error.(fields1{iter1}).(fields2{iter2}) = [p25_error.(fields1{iter1}).(fields2{iter2}); ...
                p25_error_part2a.(fields1{iter1}).(fields2{iter2}); p25_error_part2b.(fields1{iter1}).(fields2{iter2})];
            p75_error.(fields1{iter1}).(fields2{iter2}) = [p75_error.(fields1{iter1}).(fields2{iter2}); ...
                p75_error_part2a.(fields1{iter1}).(fields2{iter2}); p75_error_part2b.(fields1{iter1}).(fields2{iter2})];
        end %for
    end %for
    
    % Nominal estimators
    for iter1 = 4:6
        median_error.(fields1{iter1}) = [median_error.(fields1{iter1}); ...
            median_error_part2a.(fields1{iter1}); median_error_part2b.(fields1{iter1})];
        mean_error.(fields1{iter1})   = [mean_error.(fields1{iter1}); ...
            mean_error_part2a.(fields1{iter1}); mean_error_part2b.(fields1{iter1})];
        std_error.(fields1{iter1})    = [std_error.(fields1{iter1}); ...
            std_error_part2a.(fields1{iter1}); std_error_part2b.(fields1{iter1})];
        min_error.(fields1{iter1})    = [min_error.(fields1{iter1}); ...
            min_error_part2a.(fields1{iter1}); min_error_part2b.(fields1{iter1})];
        max_error.(fields1{iter1})    = [max_error.(fields1{iter1}); ...
            max_error_part2a.(fields1{iter1}); max_error_part2b.(fields1{iter1})];
        p25_error.(fields1{iter1})    = [p25_error.(fields1{iter1}); ...
            p25_error_part2a.(fields1{iter1}); p25_error_part2b.(fields1{iter1})];
        p75_error.(fields1{iter1})    = [p75_error.(fields1{iter1}); ...
            p75_error_part2a.(fields1{iter1}); p75_error_part2a.(fields1{iter1})];
    end %for
    
    clear median_error_part2 mean_error_part2 std_error_part2 min_error_part2 ...
        max_error_part2 p25_error_part2 p75_error_part2

    save('IEEE_14_vary_sigma_results.mat');
    
end %function