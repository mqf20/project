
function xhatrob=Estimate_Rob(Y,u,A,B,C,D,n,m,p,T,lr,lambda,rho)

cvx_solver sdpt3 %Gurobi
cvx_precision medium
cvx_begin quiet

variable xhatrob( n )
variable Dhatrob( m , T)
Phi=[];
Theta_D=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhatrob];
    sum_tmp=zeros(n,1);
    for j=0:i-2
        tmp2=i-2-j;
        if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatrob(:,j+1));
        end
    end
    Theta_D=[Theta_D D*(u(:,i)+Dhatrob(:,i))+C*sum_tmp];
end
Ehatrob=Y-Phi-Theta_D;
obj=rho*norm(xhatrob,1);
for i=1:p
    obj=obj+norm(Ehatrob(i,:),lr);
end
for i=1:m
    obj=obj+rho*norm(Dhatrob(i,:),1)+lambda*norm(Dhatrob(i,:),lr);
end
minimize( obj ) % Unconstrained minimization

cvx_end
cvx_clear

end