clear all;

n = 5;
m = 4;
p = 6;
T = 100; % Number of time steps

A = randn(n,n); B = randn(n,m);
C = randn(p,n); D = randn(p,m);

x0 = randn(n,1);

%% Inputs

d = randn(m,T);
d0 = d(:,1);

e = randn(p,T);

%% Actual system

x = x0;

for t = 1 : T
  
  Y(:,t) = C * x + D * d(:,t) + e(:,t);
  x = A * x + B * d(:,t);
  
end %for

%% Proposed system

Phi(:,1) = C * x0;

Theta(:,1) = D * d0;

Y2(:,1) = Phi(:,1) + Theta(:,1) + e(:,1);

errors(:,1) = norm(Y(:,1) - Y2(:,1));

for t = 2 : T
  
  Phi(:,t)   = C * A^(t-1) * x0;
  
  theta = zeros(p,1);
  for s = 1 : t-1
    theta = theta + C * A^(t-1-s) * B * d(:,s);
  end %for
    
  Theta(:,t) = theta + D * d(:,t);

  Y2(:,t) = Phi(:,t) + Theta(:,t) + e(:,t);
  
  errors(:,t) = norm(Y(:,t) - Y2(:,t));
  
end %for

%% Plotting

figure;
semilogy(errors); grid on;