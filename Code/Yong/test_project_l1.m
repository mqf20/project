% Problem Set 3 Problem 1 Part (c)


close all; clear all; clc;
Iter=500;
count=0;
% avg=0;
% avg_nom=0;

e_rob=[];
e_nom=[];

rho = 5; %2
lambda = 0.01; %lambda doesn't seem to make a difference same results even with lambda=0
lr=1;

n = 20;
m = 5;
p = 20;
T = n;%15; % Number of time-steps
q_s = 5;
q_a = 1;


for iter=1:Iter

display(iter)
    
x0=randn(n,1);

rm=randperm(m);
ra=rm(1:m-q_a);
G=eye(m);
G(:,ra)=[];

rp=randperm(p);
rs=rp(1:p-q_s);
H=eye(p);
H(:,rs)=[];


A = 0.1.*randn(n,n); B = randn(n,m);
C = randn(p,n); D = randn(p,m);
dA = 0.01*randn(n,n); dB = 0.1*randn(n,m);
dC = 0.1*randn(p,n); dD = 0.1*randn(p,m);
A_tilde=A+dA; B_tilde =B+dB;
C_tilde=C+dC; D_tilde =D+dD;

Q=1e-1*eye(n);%diag([0.1 0.16 0.2 0.12 0.25 0.14 0.03 0.21 0.3 0.02 0.09 0.18]);
R=1e-1*eye(p);%*diag([0.21 0.06 0.22 0.02 0.19 0.14 0.13 0.11 0.23 0.12 0.03 0.18]);

% T time steps
k=1:T;
u=ones(m,length(k));
d=ones(q_a,length(k));
for i=1:length(q_a)
    d(i,:)=i*randn.*d(i,:);
    u(i,:)=sqrt(i)*randn.*u(i,:);
end
e=ones(q_s,length(k));
for i=1:length(q_s)
    e(i,:)=i^2*randn.*e(i,:);
end


% Generate w and v
SigmaQ = chol(Q);
w = (randn(length(k),n)*SigmaQ)';
SigmaR = chol(R);
v = (randn(length(k),p)*SigmaR)';

% Dynamics
x=zeros(n,length(k));
y=zeros(p,length(k));
x(:,1)=x0;
for i=k(1:k(end))
    y(:,i)=C_tilde*x(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
    if i<k(end)
        x(:,i+1)=A_tilde*x(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
    end
end
Yvec=vec(y);
O=[];
for j=0:T-1
   O=[O;C*A^j]; 
end
% O_tilde=[];
% for j=0:T-1
%    O_tilde=[O_tilde;C_tilde*A_tilde^j]; 
% end
% dO=O_tilde-O;
J=zeros(p*T,m*T);
for i=1:T
    for j=1:i
        if i==j
            J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D;
        else
            for k=1:T-1
                if i-j==k
                    J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C*A^(k-1)*B;
                end
            end
        end
    end
end
% J_tilde=zeros(p*T,m*T);
% for i=1:T
%     for j=1:i
%         if i==j
%             J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D_tilde;
%         else
%             for k=1:T-1
%                 if i-j==k
%                     J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C_tilde*A_tilde^(k-1)*B_tilde;
%                 end
%             end
%         end
%     end
% end
% dJ=J_tilde-J;
% Jw=zeros(p*T,n*T);
% for i=1:T
%     for j=1:i        
%         for k=1:T-1
%             if i-j==k
%                 Jw(1+(i-1)*p:p+(i-1)*p,1+(j-1)*n:n+(j-1)*n)=C*A^(k-1);
%             end
%         end
%     end
% end
% noise=Jw*vec(w)+vec(v);
% dPsi=[dO J_tilde noise];



cvx_solver sdpt3 %Gurobi
cvx_precision medium
cvx_begin quiet
variable xhat( n ) 
variable dhat( m*T)

ehat=Yvec-O*xhat-J*(vec(u)+vec(dhat));
obj=norm(ehat,1)+rho*norm(xhat,1)+(rho+lambda)*norm(dhat,1);

minimize( obj )

cvx_end

% Nominal
%rho=0;
cvx_solver sdpt3 %Gurobi
cvx_precision medium
cvx_begin quiet
variable xhatnom( n ) 
variable dhatnom( m*T)

ehat=Yvec-O*xhatnom-J*(vec(u)+vec(dhatnom));
obj=norm(ehat,1)+lambda*norm(dhatnom,1);

minimize( obj )

cvx_end
  
%   fprintf('For Tau = %d, the worst case cost incurred is %f\n', tau, cvx_optval);

%   iteration=Gamma+1;
%   r{iteration}.Gamma = Gamma;
%   r{iteration}.optval = cvx_optval;
%   r{iteration}.x = x;
%   r{iteration}.y = y;
%   r{iteration}.s = s;
%   r{iteration}.t = t;

% norm1=norm(dPsi,1);
% normInf=norm(dPsi,inf);
error=norm((xhat-x0))/(norm(x0)+eps);%norm(xhat-x0);
error_nom=norm((xhatnom-x0))/(norm(x0)+eps);%norm(xhatnom-x0);
if error_nom-error<0
    count=count+1;
end
% avg=avg+error;
% avg_nom=avg_nom+error_nom;

% figure(1)
% hold all
% scatter(norm1,error,'bx')
% scatter(norm1,error_nom,'ro')
% figure(2)
% hold all
% scatter(normInf,error,'bx')
% scatter(normInf,error_nom,'ro')
e_rob=[e_rob; error];
e_nom=[e_nom; error_nom];

end
% avg=avg/Iter
% avg_nom=avg_nom/Iter
boxplot([e_rob,e_nom],'whisker',1000)
display(count)

% save('l1_l1_rho5_lambda0_01_new.mat','e_rob','e_nom','count')
display('Attack and Uncertainty')
% for i = 1 : size(r,2)
%   % Parse r
%   optval(i) = r{i}.optval;
%   Gamma(i) = r{i}.Gamma;
%   % Generate violation probabilities (from Lecture 5 page 10)
%   Phi(i) = qfunc((Gamma(i)-1)/sqrt(F*C));
%   
% end  %for
% 
% 
% % figure; 
% % plot(Gamma,optval,'*'); grid on;
% % xlabel('Gamma'); ylabel('Worst-case cost');
% 
% figure;
% plot(Gamma,Phi)
% [ax, p1, p2] = plotyy(Gamma,optval,Gamma,Phi); grid on;
% p1.LineStyle = '--';
% p1.LineWidth = 2;
% p2.LineWidth = 2;
% ylabel(ax(1),'Worst-case cost') % label left y-axis
% ylabel(ax(2),'Violation probability') % label right y-axis
% xlabel(ax(1),'\Gamma') % label x-axis
% 
