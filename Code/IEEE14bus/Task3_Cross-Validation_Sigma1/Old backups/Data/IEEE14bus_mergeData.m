% Project l1/l1 (robust)
close all; clear all; clc;

%% Set up parameters

rhoVec =  logspace(-4,2,20);
lambdaVec = 0.01:0.025:0.025*20;
nRho=length(rhoVec);
nLambda=length(lambdaVec);
nSets=20;

median_error_l1 = zeros(nLambda,1,nSets);   % (qactuators,qsensors)
mean_error_l1 = zeros(nLambda,1,nSets);
% max_error = zeros(nLambda,1,nSets);
% min_error = zeros(nLambda,1,nSets);
% p25_error = zeros(nLambda,1,nSets);
% p75_error = zeros(nLambda,1,nSets);

median_error_rob_l1 = zeros(nLambda,nRho,nSets);   % (qactuators,qsensors)
mean_error_rob_l1 = zeros(nLambda,nRho,nSets);
% max_error_rob = zeros(nLambda,nRho,nSets);
% min_error_rob = zeros(nLambda,nRho,nSets);
% p25_error_rob = zeros(nLambda,nRho,nSets);
% p75_error_rob = zeros(nLambda,nRho,nSets);

median_error_l2 = zeros(nLambda,1,nSets);   
mean_error_l2 = zeros(nLambda,1,nSets);
median_error_rob_l2 = zeros(nLambda,nRho,nSets);   
mean_error_rob_l2 = zeros(nLambda,nRho,nSets);

median_error_linf = zeros(nLambda,1,nSets);   
mean_error_linf = zeros(nLambda,1,nSets);
median_error_rob_linf = zeros(nLambda,nRho,nSets);   
mean_error_rob_linf = zeros(nLambda,nRho,nSets);


for lr=[1,2,Inf]
    for nSet=1:nSets
        name = {'l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
        str = strjoin(name,'');
        % assignin('base', strVar, 0);
        if exist(str, 'file')
            tmp=load(str);
            if lr==1
                median_error_l1=median_error_l1+tmp.median_error;
                mean_error_l1=mean_error_l1+tmp.mean_error;
                %             max_error=max_error+tmp.max_error;
                %             min_error=min_error+tmp.min_error;
                %             p25_error=p25_error+tmp.p25_error;
                %             p75_error=p75_error+tmp.p75_error;
                
                median_error_rob_l1=median_error_rob_l1+tmp.median_error_rob;
                mean_error_rob_l1=mean_error_rob_l1+tmp.mean_error_rob;
                %             max_error_rob=max_error_rob+tmp.max_error_rob;
                %             min_error_rob=min_error_rob+tmp.min_error_rob;
                %             p25_error_rob=p25_error_rob+tmp.p25_error_rob;
                %             p75_erro_robr=p75_error_rob+tmp.p75_error_rob;
            elseif lr==2
                median_error_l2=median_error_l2+tmp.median_error;
                mean_error_l2=mean_error_l2+tmp.mean_error;
                median_error_rob_l2=median_error_rob_l2+tmp.median_error_rob;
                mean_error_rob_l2=mean_error_rob_l2+tmp.mean_error_rob;
            else
                median_error_linf=median_error_linf+tmp.median_error;
                mean_error_linf=mean_error_linf+tmp.mean_error;
                median_error_rob_linf=median_error_rob_linf+tmp.median_error_rob;
                mean_error_rob_linf=mean_error_rob_linf+tmp.mean_error_rob;
            end
            
            
        end

    end
end

% m=min(A(A>0)); 

save('mergedData.mat','rhoVec','lambdaVec','median_error_l1','median_error_rob_l1','mean_error_l1','mean_error_rob_l1','median_error_l2','median_error_rob_l2','mean_error_l2','mean_error_rob_l2','median_error_linf','median_error_rob_linf','mean_error_linf','mean_error_rob_linf')
% name = {'l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
% str = strjoin(name,'');
% save(str,'tElapsed','rhoVec','lambdaVec','lr','median_error','median_error_rob','mean_error','mean_error_rob','min_error','min_error_rob','max_error','max_error_rob','p25_error','p25_error_rob','p75_error','p75_error_rob')
display('Attack+Uncertainty')
