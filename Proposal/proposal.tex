\documentclass[10pt,letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[cm]{fullpage} % Smaller margins
\setlength\parindent{0pt} % Remove paragraph indents

\begin{document}
\thispagestyle{empty}
\begin{center}
  {\Large \bfseries 15.094 Project Proposal}\\[0.5em]
  {\large Robust Estimation for Cyber-Physical Systems Under Adversarial Attacks} 
  \\[0.1em]
Ming Qing Foo (\texttt{mqf20@mit.edu}) and Sze Zheng Yong (\texttt{szyong@mit.edu})%\\
  %\today
\end{center} 

%\vspace{-0.2cm}
\section{\large Motivation/Introduction}

Cyber-Physical Systems (CPS) are systems in which computational and communication elements collaborate to control physical entities. Large-scale CPS sustains the operation of many critical processes that the modern society relies on, such as the electric power grids and water distribution networks. Attacks on these critical infrastructure can have disastrous consequences. \\

As in \cite{Fawzi14}, we consider the dynamical system that describes a CPS as a linear, time invariant (LTI) dynamical system under adversarial attack via data injection using \vspace{-0.5cm}
\begin{align*}
  x_{k+1} &= A x_{k} \\
  y_{k} &= C x_{k} + e_{k}
\end{align*}
where $e_{k}$ represents the attack vectors injected by the malicious agent in the sensors. For simplicity, we assume that $B=0$. The objective of \emph{resilient estimation} is to reconstruct the initial state $x_{0}$ of the plant from the corrupted measurements $y_{k}$, $k=0,\cdots,T-1$. A recent paper \cite{Fawzi14} proposed the following optimal estimator using $l_{0}$-``norm" minimization:
\begin{equation} \label{P}
  \min_{\hat{x} \in \mathbb{R}^{n}} \left\| Y - \Phi ( \hat{x} ) \right\|_{l_{0}}
\end{equation}
where $Y \in \mathbb{R}^{p \times T}$ is formed by concatenating the $y_{k}$'s in columns, and $\Phi$ is a linear map defined by $\Phi : \mathbb{R}^{n} \rightarrow \mathbb{R}^{p \times T}$, $ \Phi (x) = \begin{bmatrix} C x & C A x & \cdots & C A^{T-1} x \end{bmatrix}$, provided that the number of attacked sensors is fewer than $\lceil \frac{p}{2} - 1 \rceil$, and $(A, C)$ is controllable. For a matrix $M$, $\| M \|_{l_{0}}$ is the number of non-zero rows of $M$. \\

\vspace{-0.5cm}
\section{\large Problem Statement}

We propose to extend the problem considered in \cite{Fawzi14} by considering uncertain parameters $\tilde{A} := A + \Delta A$ and $\tilde{C} := C + \Delta C$, where $\Delta A$ and $\Delta C$ capture modeling errors in our system and are assumed to be in given uncertainty sets $\mathcal{U}_{A}$, and $\mathcal{U}_{C}$ respectively. The robust optimal estimator we consider is
\begin{equation} \label{RC}
  \min_{\hat{x} \in \mathbb{R}^{n}} \max_{\begin{smallmatrix}
  \Delta A \in \mathcal{U}_{A} \\ \Delta C \in \mathcal{U}_{C} \end{smallmatrix}} \left\| Y - \Phi_{\Delta A, \Delta C} ( \hat{x} ) \right\|_{l_{0}}
\end{equation}
where $\Phi_{\Delta A, \Delta C} ( \cdot )$ is defined similarly to $\Phi ( \cdot )$ but with uncertain parameters. The objective remains is to reconstruct the initial state $x_{0}$ of the plant from the corrupted measurements $y_{k}$, $k=0,\cdots,T-1$, but in a robust manner.\\

\vspace{-0.5cm}
\section{\large Plan of attack}

The minimization problem in (\ref{P}) is intractable due to the non-convexity of the $l_{0}$-``norm" operator, which motivated the relaxation considered by \cite{Fawzi14} to a $l_{1}/l_{r}$ minimization that has been shown to perform just as well under certain assumptions, where $\| M \|_{l_{1}/l_{r}}$ is the sum of the $l_{r}$ norms of the rows of the matrix $M$. This motivates us to propose a similar $l_{1}/l_{r}$ estimator for (\ref{RC}):  
\begin{equation} \label{RC2}
  \min_{\hat{x} \in \mathbb{R}^{n}} \max_{\begin{smallmatrix}
  \Delta A \in \mathcal{U}_{A} \\ \Delta C \in \mathcal{U}_{C} \end{smallmatrix}} \left\| Y - \Phi_{\Delta A, \Delta C} ( \hat{x} ) \right\|_{l_{1}/l_{r}}
\end{equation}

We will attempt to recast the robust regression problem (\ref{RC3})  as a regularized regression problem:
\begin{equation} \label{RC3}
  \min_{\hat{x} \in \mathbb{R}^{n}}\left\| Y - \Phi_{\Delta A, \Delta C} ( \hat{x} ) \right\|_{l_{1}/l_{r}} + \rho \|\hat{x}\|_{l_1}
%  \min_{\hat{x} \in \mathbb{R}^{n}} \max_{\begin{smallmatrix}
%  \Delta A \in \mathcal{U}_{A} \\ \Delta C \in \mathcal{U}_{C} \end{smallmatrix}} \left\| \overline{Y} - \left( \overline{\Phi} + \Delta \Phi_{\Delta A, \Delta C} \right) \hat{x} \right\|_{l_{1}}
\end{equation}
for some $\rho$ that is a function of the bounds on $\Delta A$ and $\Delta C$. To test our proposed methods, we plan to implement our robust estimator on a model of an electric power network \cite{Wood96}. We shall compare the performances of our estimator and the estimator in \cite{Fawzi14} (i.e., using the nominal matrices $A$ and $C$) in terms of their deviations from the true initial state $x_0$.

\vspace{-0.15cm} \small
\bibliographystyle{plain} 
\bibliography{sources}

\include{appendix}

\end{document}