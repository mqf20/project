close all; clear all; clc;

%% Set up problem

max_iters = 1; % Maximum number of iterations
count = 0;
avg = 0;
avg_nom = 0;

rho = 1;
lambda = 0.01; % For l2, lambda=[5,15] (10), % For l_inf, lambda=[0.05,1] (0.5)
lr = 2; % 2 and inf, crashes occasionally when I use lr=1

n = 20; % Number of states
m = 5; % Number of inputs
p = 15; % Number of outputs
T = n; % 15; % Number of time-steps
q_s = 5; % Number of attacked sensors
q_a = 1; % Number of attacked actuators


for iter=1:max_iters
  
    %% Generate parameters

    x0 = randn(n,1); % Initial state

    % Actuator attacks
    rm = randperm(m);
    ra = rm(1:m-q_a); % Actuators that are NOT attacked
    G = eye(m);
    G(:,ra) = []; % Actuator attack matrix

    % Sensor attacks
    rp = randperm(p);
    rs = rp(1:p-q_s); % Sensors that are NOT attacked
    H = eye(p);
    H(:,rs) = []; % Sensor attack matrix

    % Actual system dynamics  
    A = 0.1.*randn(n,n); B = randn(n,m);
    C = randn(p,n); D = randn(p,m);
    dA = 0.01*randn(n,n); dB = 0.1*randn(n,m);
    dC = 0.1*randn(p,n); dD = 0.1*randn(p,m);
    A_tilde=A+dA; B_tilde =B+dB;
    C_tilde=C+dC; D_tilde =D+dD;

    Q=1e-1*eye(n);%diag([0.1 0.16 0.2 0.12 0.25 0.14 0.03 0.21 0.3 0.02 0.09 0.18]);
    R=1e-1*eye(p);%*diag([0.21 0.06 0.22 0.02 0.19 0.14 0.13 0.11 0.23 0.12 0.03 0.18]);

    % T time steps
    k=1:T;

    %% Generate attacks

    d=randn(q_a,T); % Actuator attacks
    e=randn(q_s,T); % Sensor attacks

    %% Generate noises

    SigmaQ = chol(Q);
    w = (randn(T,n)*SigmaQ)';
    SigmaR = chol(R);
    v = (randn(T,p)*SigmaR)';

    %% Dynamics
    %  Assuming zero inputs, but there may still be actuator attacks

    X=zeros(n,T); % State
    Y=zeros(p,T); % Output
    X(:,1)=x0;
    for i=k(1:k(end))
        Y(:,i)=C_tilde*X(:,i)+D_tilde*G*d(:,i)+H*e(:,i)+v(:,i);
        if i<k(end)
            X(:,i+1)=A_tilde*X(:,i)+B_tilde*G*d(:,i)+w(:,i);
        end
    %     % no uncertainties
    %     y(:,i)=C*x(:,i)+D*G*d(:,i)+H*e(:,i);
    %     if i<k(end)
    %         x(:,i+1)=A*x(:,i)+B*G*d(:,i);
    %     end
    end

    keyboard
    
    %% Robust estimator

    cvx_solver Gurobi
    cvx_begin
    
        variable xhat( n ) 
        variable Dhat( m , T)
        
        % Compute variable Phi
        Phi=[];
        % Theta=[];
        for i=1:T
            Phi=[Phi C*A^(i-1)*xhat];
        %     sum_tmp=zeros(n,1);
        %     for j=0:T-2
        %        tmp2=i-2-j;
        %        if tmp2>=0
        %             sum_tmp=sum_tmp+A^(tmp2)*B*Dhat(:,j+1); 
        %        end
        %     end
        %     Theta=[Theta D*Dhat(:,i)+C*sum_tmp];
        end
        
        Ehat=Y-Phi;%-Theta;
        
        obj=rho*norm(xhat,1);
        
        for i=1:p
            obj=obj+norm(Ehat(i,:),lr);
        end
        
        for i=1:m
            obj=obj+rho*norm(Dhat(i,:),1)+lambda*norm(Dhat(i,:),lr);
        end
        
        minimize( obj ) % Unconstrained minimization

    cvx_end

    cvx_clear

    % Nominal
    %rho=0;
    cvx_solver Gurobi
    cvx_begin
    variable xhatnom( n ) 
    variable Dhatnom( m , T)
    Phi=[];
    % Theta=[];
    for i=1:T
        Phi=[Phi C*A^(i-1)*xhatnom];
    %     sum_tmp=zeros(n,1);
    %     for j=0:T-2
    %        tmp2=i-2-j;
    %        if tmp2>=0
    %             sum_tmp=sum_tmp+A^(tmp2)*B*Dhatnom(:,j+1); 
    %        end
    %     end
    %     Theta=[Theta D*Dhatnom(:,i)+C*sum_tmp];
    end
    Ehat=Y-Phi;%-Theta;
    obj=0;%rho*norm(xhatnom,1);
    for i=1:p
        obj=obj+norm(Ehat(i,:),lr);
    end
    for i=1:m
        obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
    end
    minimize( obj )

    cvx_end

    cvx_clear

    %   fprintf('For Tau = %d, the worst case cost incurred is %f\n', tau, cvx_optval);

    %   iteration=Gamma+1;
    %   r{iteration}.Gamma = Gamma;
    %   r{iteration}.optval = cvx_optval;
    %   r{iteration}.x = x;
    %   r{iteration}.y = y;
    %   r{iteration}.s = s;
    %   r{iteration}.t = t;

    O=[];
    for j=0:T-1
       O=[O;C*A^j]; 
    end
    O_tilde=[];
    for j=0:T-1
       O_tilde=[O_tilde;C_tilde*A_tilde^j]; 
    end
    dO=O_tilde-O;
    J=zeros(p*T,m*T);
    for i=1:T
        for j=1:i
            if i==j
                J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D;
            else
                for k=1:T-1
                    if i-j==k
                        J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C*A^(k-1)*B;
                    end
                end
            end
        end
    end
    J_tilde=zeros(p*T,m*T);
    for i=1:T
        for j=1:i
            if i==j
                J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D_tilde;
            else
                for k=1:T-1
                    if i-j==k
                        J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C_tilde*A_tilde^(k-1)*B_tilde;
                    end
                end
            end
        end
    end
    dJ=J_tilde-J;
    Jw=zeros(p*T,n*T);
    for i=1:T
        for j=1:i        
            for k=1:T-1
                if i-j==k
                    Jw(1+(i-1)*p:p+(i-1)*p,1+(j-1)*n:n+(j-1)*n)=C*A^(k-1);
                end
            end
        end
    end
    noise=Jw*vec(w)+vec(v);
    dPsi=[dO J_tilde noise];

    norm1=norm(dPsi,1);
    normInf=norm(dPsi,inf);
    error=norm(xhat-x0);
    error_nom=norm(xhatnom-x0);
    if error_nom-error<0
        count=count+1;
    end
    avg=avg+error;
    avg_nom=avg_nom+error_nom;

    figure(1)
    hold all
    scatter(norm1,error,'bx')
    scatter(norm1,error_nom,'ro')
    figure(2)
    hold all
    scatter(normInf,error,'bx')
    scatter(normInf,error_nom,'ro')

end
avg=avg/max_iters
avg_nom=avg_nom/max_iters
count

% for i = 1 : size(r,2)
%   % Parse r
%   optval(i) = r{i}.optval;
%   Gamma(i) = r{i}.Gamma;
%   % Generate violation probabilities (from Lecture 5 page 10)
%   Phi(i) = qfunc((Gamma(i)-1)/sqrt(F*C));
%   
% end  %for
% 
% 
% % figure; 
% % plot(Gamma,optval,'*'); grid on;
% % xlabel('Gamma'); ylabel('Worst-case cost');
% 
% figure;
% plot(Gamma,Phi)
% [ax, p1, p2] = plotyy(Gamma,optval,Gamma,Phi); grid on;
% p1.LineStyle = '--';
% p1.LineWidth = 2;
% p2.LineWidth = 2;
% ylabel(ax(1),'Worst-case cost') % label left y-axis
% ylabel(ax(2),'Violation probability') % label right y-axis
% xlabel(ax(1),'\Gamma') % label x-axis
% 
