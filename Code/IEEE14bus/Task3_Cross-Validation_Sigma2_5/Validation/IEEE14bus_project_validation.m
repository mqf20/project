% Project l1/linf (robust)
close all; clear all; clc;

%% Set up parameters

load IEEE14bus_DataSets.mat
load mergedData.mat

nSet_new=[];
for ii=1:nSets
    name = {'l1_lr_Valid_nSet',num2str(ii),'.mat'};
    str = strjoin(name,'');
    if ~exist(str, 'file') && mean_error_l1(1,1,ii)~=0
        nSet_new=[nSet_new,ii];
    end
end

nValid=0.25*nData;

O=[];
for j=0:T-1
    O=[O;C*A^j];
end

J=zeros(p*T,m*T);
for i=1:T
    for j=1:i
        if i==j
            J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D;
        else
            for k=1:T-1
                if i-j==k
                    J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C*A^(k-1)*B;
                end
            end
        end
    end
end

for nSet=nSet_new

    lambdaNom=zeros(3,nSets); % 1: l1, 2: l2, 3: linf
    lambdaRob=zeros(3,nSets);
    rhoRob=zeros(3,nSets);
    
    minNom=zeros(3,nSets);
    minRob=zeros(3,nSets);
    
    median_error_V = zeros(3,nSets);   % 1: l1, 2: l2, 3: linf
    mean_error_V = zeros(3,nSets);
    std_error_V = zeros(3,nSets);
    max_error_V = zeros(3,nSets);
    min_error_V = zeros(3,nSets);
    p25_error_V = zeros(3,nSets);
    p75_error_V = zeros(3,nSets);
    
    median_error_rob_V = zeros(3,nSets);   % 1: l1, 2: l2, 3: linf
    mean_error_rob_V = zeros(3,nSets);
    std_error_rob_V = zeros(3,nSets);
    max_error_rob_V = zeros(3,nSets);
    min_error_rob_V = zeros(3,nSets);
    p25_error_rob_V = zeros(3,nSets);
    p75_error_rob_V = zeros(3,nSets);
    
    lr_median_error_V = zeros(3,nSets);   % 1: l1, 2: l2, 3: linf; lambda; min
    lr_mean_error_V = zeros(3,nSets);
    lr_std_error_V = zeros(3,nSets);
    lr_max_error_V = zeros(3,nSets);
    lr_min_error_V = zeros(3,nSets);
    lr_p25_error_V = zeros(3,nSets);
    lr_p75_error_V = zeros(3,nSets);
    
    lr_median_error_rob_V = zeros(4,nSets);   % 1: l1, 2: l2, 3: linf; lambda; rho; min
    lr_mean_error_rob_V = zeros(4,nSets);
    lr_std_error_rob_V = zeros(4,nSets);
    lr_max_error_rob_V = zeros(4,nSets);
    lr_min_error_rob_V = zeros(4,nSets);
    lr_p25_error_rob_V = zeros(4,nSets);
    lr_p75_error_rob_V = zeros(4,nSets);
    
tStart = tic;

for kkp=nSet%1:nSet
    
%     load l1_l1_Train.mat
    tmp=median_error_l1(:,:,kkp);
    tmp(tmp==0)=Inf;
    [minNom(1,kkp),ind]=min(tmp);
    lambdaNom(1,kkp)=lambdaVec(ind);
    tmp=median_error_rob_l1(:,:,kkp);
    tmp(tmp==0)=Inf;
    [min_i,argmin_i]=min(tmp);
    [minRob(1,kkp),ind]=min(min_i);
    rhoRob(1,kkp)=rhoVec(ind);
    ind2=argmin_i(ind);
    lambdaRob(1,kkp)=lambdaVec(ind2);
    
    tmp=median_error_l2(:,:,kkp);
    tmp(tmp==0)=Inf;
    [minNom(2,kkp),ind]=min(tmp);
    lambdaNom(2,kkp)=lambdaVec(ind);
    tmp=median_error_rob_l2(:,:,kkp);
    tmp(tmp==0)=Inf;
    [min_i,argmin_i]=min(tmp);
    [minRob(2,kkp),ind]=min(min_i);
    rhoRob(2,kkp)=rhoVec(ind);
    ind2=argmin_i(ind);
    lambdaRob(2,kkp)=lambdaVec(ind2);
    
    tmp=median_error_linf(:,:,kkp);
    tmp(tmp==0)=Inf;
    [minNom(3,kkp),ind]=min(tmp);
    lambdaNom(3,kkp)=lambdaVec(ind);
    tmp=median_error_rob_linf(:,:,kkp);
    tmp(tmp==0)=Inf;
    [min_i,argmin_i]=min(tmp);
    [minRob(3,kkp),ind]=min(min_i);
    rhoRob(3,kkp)=rhoVec(ind);
    ind2=argmin_i(ind);
    lambdaRob(3,kkp)=lambdaVec(ind2);
    
%     load l1_l2_Train.mat
%     [minNom(2,kkp),lambdaNom(2,kkp)]=min(median_error(:,:,kkp));
%     [min_i,argmin_i]=min(median_error_rob(:,:,kkp));
%     [minRob(2,kkp),rhoRob(2,kkp)]=min(min_i);
%     lambdaRob(2,kkp)=argmin_i(rhoRob(2,kkp));
%     
%     load l1_linf_Train.mat
%     [minNom(3,kkp),lambdaNom(3,kkp)]=min(median_error(:,:,kkp));
%     [min_i,argmin_i]=min(median_error_rob(:,:,kkp));
%     [minRob(3,kkp),rhoRob(3,kkp)]=min(min_i);
%     lambdaRob(3,kkp)=argmin_i(rhoRob(3,kkp));
%     for kka=1:nLambda,
        
        %% Nominal
        lambda=lambdaNom(1,kkp);
        e_nom=[];
        %     display(kka)
        
        for kkk=1:nValid
                    display(kkk)
            Y=YMat_ValidSet(:,:,kkk,kkp);
            u=UMat_ValidSet(:,:,kkk,kkp);
            x0=XMat_ValidSet(:,:,kkk,kkp);
            
            
            Yvec=vec(Y);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatnom( n )
            variable dhatnom( m*T)
            
            ehat=Yvec-O*xhatnom-J*(vec(u)+vec(dhatnom));
            obj=norm(ehat,1)+lambda*norm(dhatnom,1);
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom=[e_nom; error_nom];
            
        end
        median_error_V(1,kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error_V(1,kkp) = mean(e_nom);
        std_error_V(1,kkp) = std(e_nom);
        max_error_V(1,kkp) = max(e_nom);
        min_error_V(1,kkp) = min(e_nom);
        p25_error_V(1,kkp) = prctile(e_nom,25);
        p75_error_V(1,kkp) = prctile(e_nom,75);
        
        
        lambda=lambdaNom(2,kkp);
        lr=2;
        e_nom=[];
        
        for kkk=1:nValid
            display(kkk)
            Y=YMat_ValidSet(:,:,kkk,kkp);
            u=UMat_ValidSet(:,:,kkk,kkp);
            x0=XMat_ValidSet(:,:,kkk,kkp);
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatnom( n )
            variable Dhatnom( m , T)
            Phi=[];
            Theta_D=[];
            for i=1:T
                Phi=[Phi C*A^(i-1)*xhatnom];
                sum_tmp=zeros(n,1);
                for j=0:i-2
                    tmp2=i-2-j;
                    if tmp2>=0
                        sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1));
                    end
                end
                Theta_D=[Theta_D D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
            end
            Ehat=Y-Phi-Theta_D;
            obj=0;%rho*norm(xhatnom,1);
            for i=1:p
                obj=obj+norm(Ehat(i,:),lr);
            end
            for i=1:m
                obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
            end
            % obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom=[e_nom; error_nom];
            
        end
        median_error_V(2,kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error_V(2,kkp) = mean(e_nom);
        std_error_V(2,kkp) = std(e_nom);
        max_error_V(2,kkp) = max(e_nom);
        min_error_V(2,kkp) = min(e_nom);
        p25_error_V(2,kkp) = prctile(e_nom,25);
        p75_error_V(2,kkp) = prctile(e_nom,75);
        
        lambda=lambdaNom(3,kkp);
        lr=inf;
        e_nom=[];
        
        for kkk=1:nValid
            display(kkk)
            Y=YMat_ValidSet(:,:,kkk,kkp);
            u=UMat_ValidSet(:,:,kkk,kkp);
            x0=XMat_ValidSet(:,:,kkk,kkp);
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatnom( n )
            variable Dhatnom( m , T)
            Phi=[];
            Theta_D=[];
            for i=1:T
                Phi=[Phi C*A^(i-1)*xhatnom];
                sum_tmp=zeros(n,1);
                for j=0:i-2
                    tmp2=i-2-j;
                    if tmp2>=0
                        sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1));
                    end
                end
                Theta_D=[Theta_D D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
            end
            Ehat=Y-Phi-Theta_D;
            obj=0;%rho*norm(xhatnom,1);
            for i=1:p
                obj=obj+norm(Ehat(i,:),lr);
            end
            for i=1:m
                obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
            end
            % obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom=[e_nom; error_nom];
            
        end
        median_error_V(3,kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error_V(3,kkp) = mean(e_nom);
        std_error_V(3,kkp) = std(e_nom);
        max_error_V(3,kkp) = max(e_nom);
        min_error_V(3,kkp) = min(e_nom);
        p25_error_V(3,kkp) = prctile(e_nom,25);
        p75_error_V(3,kkp) = prctile(e_nom,75);
        
        [lr_median_error_V(end,kkp),lr_median_error_V(1,kkp)]=min(median_error_V(:,kkp));
        lr_median_error_V(2,kkp)=lambdaNom(lr_median_error_V(1,kkp),kkp);
        
        [lr_mean_error_V(end,kkp),lr_mean_error_V(1,kkp)]=min(mean_error_V(:,kkp));
        lr_mean_error_V(2,kkp)=lambdaNom(lr_mean_error_V(1,kkp),kkp);
        
        [lr_std_error_V(end,kkp),lr_std_error_V(1,kkp)]=min(std_error_V(:,kkp));
        lr_std_error_V(2,kkp)=lambdaNom(lr_std_error_V(1,kkp),kkp);
        
        [lr_max_error_V(end,kkp),lr_max_error_V(1,kkp)]=min(max_error_V(:,kkp));
        lr_max_error_V(2,kkp)=lambdaNom(lr_max_error_V(1,kkp),kkp);
        
        [lr_min_error_V(end,kkp),lr_min_error_V(1,kkp)]=min(min_error_V(:,kkp));
        lr_min_error_V(2,kkp)=lambdaNom(lr_min_error_V(1,kkp),kkp);
        
        [lr_p25_error_V(end,kkp),lr_p25_error_V(1,kkp)]=min(p25_error_V(:,kkp));
        lr_p25_error_V(2,kkp)=lambdaNom(lr_p25_error_V(1,kkp),kkp);
        
        [lr_p75_error_V(end,kkp),lr_p75_error_V(1,kkp)]=min(p75_error_V(:,kkp));
        lr_p75_error_V(2,kkp)=lambdaNom(lr_p75_error_V(1,kkp),kkp);
        
        %% Robust
        lambda=lambdaRob(1,kkp);
        rho=rhoRob(1,kkp);
        e_rob=[];
        %     display(kka)
        
        for kkk=1:nValid
                    display(kkk)
            Y=YMat_ValidSet(:,:,kkk,kkp);
            u=UMat_ValidSet(:,:,kkk,kkp);
            x0=XMat_ValidSet(:,:,kkk,kkp);
            
            Yvec=vec(Y);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatrob( n )
            variable dhatrob( m , T)
            ehatrob=Yvec-O*xhatrob-J*(vec(u)+vec(dhatrob));
            obj=norm(ehatrob,1)+rho*norm(xhatrob,1)+(rho+lambda)*norm(dhatrob,1);
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
            e_rob=[e_rob; error_rob];
            
        end
        
        median_error_rob_V(1,kkp) = median(e_rob);   % (qactuators,qsensors)
        mean_error_rob_V(1,kkp) = mean(e_rob);
        std_error_rob_V(1,kkp) = std(e_rob);
        max_error_rob_V(1,kkp) = max(e_rob);
        min_error_rob_V(1,kkp) = min(e_rob);
        p25_error_rob_V(1,kkp) = prctile(e_rob,25);
        p75_error_rob_V(1,kkp) = prctile(e_rob,75);
        
        lambda=lambdaRob(2,kkp);
        rho=rhoRob(2,kkp);
        lr=2;
        e_rob=[];
        %     display(kka)
        
        for kkk=1:nValid
                    display(kkk)
            Y=YMat_ValidSet(:,:,kkk,kkp);
            u=UMat_ValidSet(:,:,kkk,kkp);
            x0=XMat_ValidSet(:,:,kkk,kkp);
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatrob( n )
            variable Dhatrob( m , T)
            Phi=[];
            Theta_D=[];
            for i=1:T
                Phi=[Phi C*A^(i-1)*xhatrob];
                sum_tmp=zeros(n,1);
                for j=0:i-2
                    tmp2=i-2-j;
                    if tmp2>=0
                        sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatrob(:,j+1));
                    end
                end
                Theta_D=[Theta_D D*(u(:,i)+Dhatrob(:,i))+C*sum_tmp];
            end
            Ehatrob=Y-Phi-Theta_D;
            obj=rho*norm(xhatrob,1);
            for i=1:p
                obj=obj+norm(Ehatrob(i,:),lr);
            end
            for i=1:m
                obj=obj+rho*norm(Dhatrob(i,:),1)+lambda*norm(Dhatrob(i,:),lr);
            end
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
            e_rob=[e_rob; error_rob];
                
        end
        
        median_error_rob_V(2,kkp) = median(e_rob);   % (qactuators,qsensors)
        mean_error_rob_V(2,kkp) = mean(e_rob);
        std_error_rob_V(2,kkp) = std(e_rob);
        max_error_rob_V(2,kkp) = max(e_rob);
        min_error_rob_V(2,kkp) = min(e_rob);
        p25_error_rob_V(2,kkp) = prctile(e_rob,25);
        p75_error_rob_V(2,kkp) = prctile(e_rob,75);
        
        lambda=lambdaRob(3,kkp);
        rho=rhoRob(3,kkp);
        lr=inf;
        e_rob=[];
        %     display(kka)
        
        for kkk=1:nValid
                    display(kkk)
            Y=YMat_ValidSet(:,:,kkk,kkp);
            u=UMat_ValidSet(:,:,kkk,kkp);
            x0=XMat_ValidSet(:,:,kkk,kkp);
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatrob( n )
            variable Dhatrob( m , T)
            Phi=[];
            Theta_D=[];
            for i=1:T
                Phi=[Phi C*A^(i-1)*xhatrob];
                sum_tmp=zeros(n,1);
                for j=0:i-2
                    tmp2=i-2-j;
                    if tmp2>=0
                        sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatrob(:,j+1));
                    end
                end
                Theta_D=[Theta_D D*(u(:,i)+Dhatrob(:,i))+C*sum_tmp];
            end
            Ehatrob=Y-Phi-Theta_D;
            obj=rho*norm(xhatrob,1);
            for i=1:p
                obj=obj+norm(Ehatrob(i,:),lr);
            end
            for i=1:m
                obj=obj+rho*norm(Dhatrob(i,:),1)+lambda*norm(Dhatrob(i,:),lr);
            end
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
            e_rob=[e_rob; error_rob];
            
        end
        
        median_error_rob_V(3,kkp) = median(e_rob);   % (qactuators,qsensors)
        mean_error_rob_V(3,kkp) = mean(e_rob);
        std_error_rob_V(3,kkp) = std(e_rob);
        max_error_rob_V(3,kkp) = max(e_rob);
        min_error_rob_V(3,kkp) = min(e_rob);
        p25_error_rob_V(3,kkp) = prctile(e_rob,25);
        p75_error_rob_V(3,kkp) = prctile(e_rob,75);
        
        
        [lr_median_error_rob_V(end,kkp),lr_median_error_rob_V(1,kkp)]=min(median_error_rob_V(:,kkp));
        lr_median_error_rob_V(2,kkp)=lambdaRob(lr_median_error_rob_V(1,kkp),kkp);
        lr_median_error_rob_V(3,kkp)=rhoRob(lr_median_error_rob_V(1,kkp),kkp);
        
        [lr_mean_error_rob_V(end,kkp),lr_mean_error_rob_V(1,kkp)]=min(mean_error_rob_V(:,kkp));
        lr_mean_error_rob_V(2,kkp)=lambdaRob(lr_mean_error_rob_V(1,kkp),kkp);
        lr_mean_error_rob_V(3,kkp)=rhoRob(lr_mean_error_rob_V(1,kkp),kkp);
        
        [lr_std_error_rob_V(end,kkp),lr_std_error_rob_V(1,kkp)]=min(std_error_rob_V(:,kkp));
        lr_std_error_rob_V(2,kkp)=lambdaRob(lr_std_error_rob_V(1,kkp),kkp);
        lr_std_error_rob_V(3,kkp)=rhoRob(lr_std_error_rob_V(1,kkp),kkp);
        
        [lr_max_error_rob_V(end,kkp),lr_max_error_rob_V(1,kkp)]=min(max_error_rob_V(:,kkp));
        lr_max_error_rob_V(2,kkp)=lambdaRob(lr_max_error_rob_V(1,kkp),kkp);
        lr_max_error_rob_V(3,kkp)=rhoRob(lr_max_error_rob_V(1,kkp),kkp);
        
        [lr_min_error_rob_V(end,kkp),lr_min_error_rob_V(1,kkp)]=min(min_error_rob_V(:,kkp));
        lr_min_error_rob_V(2,kkp)=lambdaRob(lr_min_error_rob_V(1,kkp),kkp);
        lr_min_error_rob_V(3,kkp)=rhoRob(lr_min_error_rob_V(1,kkp),kkp);
        
        [lr_p25_error_rob_V(end,kkp),lr_p25_error_rob_V(1,kkp)]=min(p25_error_rob_V(:,kkp));
        lr_p25_error_rob_V(2,kkp)=lambdaRob(lr_p25_error_rob_V(1,kkp),kkp);
        lr_p25_error_rob_V(3,kkp)=rhoRob(lr_p25_error_rob_V(1,kkp),kkp);
        
        [lr_p75_error_rob_V(end,kkp),lr_p75_error_rob_V(1,kkp)]=min(p75_error_rob_V(:,kkp));
        lr_p75_error_rob_V(2,kkp)=lambdaRob(lr_p75_error_rob_V(1,kkp),kkp);
        lr_p75_error_rob_V(3,kkp)=rhoRob(lr_p75_error_rob_V(1,kkp),kkp);
        
        
end

tElapsed = toc(tStart);

name = {'l1_lr_Valid_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
% save('l1_lr_Valid.mat','tElapsed','lr_median_error_V','lr_mean_error_V','lr_median_error_rob_V','lr_mean_error_rob_V')
save(str,'tElapsed','lr_median_error_V','lr_mean_error_V','lr_std_error_V','lr_max_error_V','lr_min_error_V','lr_p25_error_V','lr_p75_error_V','lr_median_error_rob_V','lr_mean_error_rob_V','lr_std_error_rob_V','lr_max_error_rob_V','lr_min_error_rob_V','lr_p25_error_rob_V','lr_p75_error_rob_V')
end

display('Attack+Uncertainty')
