% Project l1/l2

close all; clear all; clc;

%% Set up parameters

load Atilde.mat
load Ctilde.mat

Ts = 0.05; % Discretization time step
n = size(Atilde,1);
Btilde = [zeros(n/2,n/2); eye(n/2,n/2)]; m = size(Btilde,2);
C = Ctilde; p = size(C,1);
Mexp=expm([Atilde Btilde; zeros(m,n+m)]*Ts);
A=Mexp(1:n,1:n);
B=Mexp(1:n,n+1:end);
D = zeros(p,m);

T = 1.5*n; 

Q=[eps*eye(n/2),zeros(n/2,n/2);zeros(n/2,n/2), 1e-1*eye(n/2)];
R=1e-1*eye(p);

nData=200;
XMat=zeros(n,1,nData);
YMat=zeros(p,T,nData);
UMat=zeros(m,T,nData);
      
q_a = 3; 
q_s = 1; 

for j=1:nData
    
    x0=randn(n,1);
    
    rm=randperm(m);
    ra=rm(1:m-q_a); % Actuators that are NOT attacked
    G=eye(m);
    G(:,ra)=[]; % Actuator attack matrix
    
    rp=randperm(p-1);
    rs=rp(1:p-1-q_s); % Sensors that are NOT attacked
    H=eye(p);
    H(:,rs)=[]; % Sensor attack matrix
    H(:,end)=[]; % the last measurement (the rotor angle) cannot be attacked
    
    dC = [0.1*randn(p,n/2), zeros(p,n/2)]; dD = 0;
    C_tilde=C+dC; D_tilde =D+dD;
    dA = [zeros(n/2,n); 0.01*randn(n/2,n/2), zeros(n/2,n/2)];
    dB = [zeros(n/2,m);diag(0.1*randn(m,1))];
    
    Mexp_tilde=expm([Atilde+dA Btilde+dB; zeros(m,n+m)]*Ts);
    A_tilde=Mexp_tilde(1:n,1:n);
    B_tilde=Mexp_tilde(1:n,n+1:end);
    
    while ~(all(abs(eig(A))<=1+1e-10) && all(abs(eig(A_tilde))<1+1e-10)) % If unstable
        dA = [zeros(n/2,n); 0.01*randn(n/2,n/2), zeros(n/2,n/2)];
        A_tilde = expm((Atilde+dA)*Ts);
        display(j)
    end
    
    % Attacks and noises
    % Generate attacks
    attackpower = 1;
    d=attackpower*randn(q_a,T);
    e=attackpower*randn(q_s,T);
    u=0.01*randn(m,T);
    
    % Generate noises
    SigmaQ = chol(Q);
    w = (randn(T,n)*SigmaQ)';
    SigmaR = chol(R);
    v = (randn(T,p)*SigmaR)';
    
    % Actual dynamics
    X=zeros(n,T);
    Y=zeros(p,T);
    X(:,1)=x0;
    for i=1:T
        Y(:,i)=C_tilde*X(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
        if i<T
            X(:,i+1)=A_tilde*X(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
        end
    end
    
    XMat(:,:,j)=x0;
    YMat(:,:,j)=Y;
    UMat(:,:,j)=u;

end

save('IEEE14bus_Data.mat','XMat','YMat','UMat','n','m','p','q_a','q_s','T','A','B','C','D','nData')
display('Attack+Uncertainty')

