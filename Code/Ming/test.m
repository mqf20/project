clear;

load('matpower.mat');
case_data = conversion(case_data);

n = numel(case_data.generators);  % Number of generators
m = case_data.num_bus - n;  % Number of load buses

