

xrob=zeros(n,length(k));
% y=zeros(p,length(k));
xrob(:,1)=xhat;
for i=k(1:k(end))
%     y(:,i)=C_tilde*xrob(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
    if i<k(end)
        xrob(:,i+1)=A*xrob(:,i)+B*(u(:,i)+Dhat(:,i));%+w(:,i);
    end
%     % no uncertainties
%     y(:,i)=C*x(:,i)+D*G*d(:,i)+H*e(:,i);
%     if i<k(end)
%         x(:,i+1)=A*x(:,i)+B*G*d(:,i);
%     end
end

xnom=zeros(n,length(k));
% y=zeros(p,length(k));
xnom(:,1)=xhatnom;
for i=k(1:k(end))
%     y(:,i)=C_tilde*xrob(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
    if i<k(end)
        xnom(:,i+1)=A*xnom(:,i)+B*(u(:,i)+Dhatnom(:,i));%+w(:,i);
    end
%     % no uncertainties
%     y(:,i)=C*x(:,i)+D*G*d(:,i)+H*e(:,i);
%     if i<k(end)
%         x(:,i+1)=A*x(:,i)+B*G*d(:,i);
%     end
end

figure
err_rob=norms(x-xrob,1)./norms(x,1);
err_nom=norms(x-xnom,1)./norms(x,1);
plot(k,err_rob,k,err_nom)
% j=10;
% plot(k,x(j,:),k,xrob(j,:),k,xnom(j,:))