% Project l1/l1 (robust)
close all; clear all; clc;

%% Set up parameters

load ../../IEEE14bus_DataSets.mat

nSet=19;

lambdaVec =  logspace(-4,2,20);
rhoVec = 0.01:0.025:0.025*20;
lr=1;%inf; % 2 and inf, crashes occasionally when I use lr=1

nTrain=0.5*nData;
nRho=length(rhoVec);
nLambda=length(lambdaVec);

median_error = zeros(nLambda,1,nSets);   % (qactuators,qsensors)
mean_error = zeros(nLambda,1,nSets);
max_error = zeros(nLambda,1,nSets);
min_error = zeros(nLambda,1,nSets);
p25_error = zeros(nLambda,1,nSets);
p75_error = zeros(nLambda,1,nSets);

median_error_rob = zeros(nLambda,nRho,nSets);   % (qactuators,qsensors)
mean_error_rob = zeros(nLambda,nRho,nSets);
max_error_rob = zeros(nLambda,nRho,nSets);
min_error_rob = zeros(nLambda,nRho,nSets);
p25_error_rob = zeros(nLambda,nRho,nSets);
p75_error_rob = zeros(nLambda,nRho,nSets);

O=[];
for j=0:T-1
    O=[O;C*A^j];
end

J=zeros(p*T,m*T);
for i=1:T
    for j=1:i
        if i==j
            J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D;
        else
            for k=1:T-1
                if i-j==k
                    J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C*A^(k-1)*B;
                end
            end
        end
    end
end

tStart = tic;

for kkp=nSet
    for kka=1:nLambda,
        
        lambda = lambdaVec(kka);
        e_nom=[];
        %     display(kka)
        
        for kkk=1:nTrain
            %         display(kkk)
            Y=YMat_TrainSet(:,:,kkk,kkp);
            u=UMat_TrainSet(:,:,kkk,kkp);
            x0=XMat_TrainSet(:,:,kkk,kkp);
            
            Yvec=vec(Y);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatnom( n )
            variable dhatnom( m*T)
            
            ehat=Yvec-O*xhatnom-J*(vec(u)+vec(dhatnom));
            obj=norm(ehat,1)+lambda*norm(dhatnom,1);
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom=[e_nom; error_nom];
            
        end
        median_error(kka,1,kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error(kka,1,kkp) = mean(e_nom);
        max_error(kka,1,kkp) = max(e_nom);
        min_error(kka,1,kkp) = min(e_nom);
        p25_error(kka,1,kkp) = prctile(e_nom,25);
        p75_error(kka,1,kkp) = prctile(e_nom,75);
        
        for kks=1:nRho,
            
            %         display(kks)
            rho = rhoVec(kks);
            e_rob=[];
            %         display(lambda)
            %         display(rho)
            
            for kkk=1:nTrain
                
                %             display(kkk)
                % l1/lr robust estimator with rho
                Y=YMat_TrainSet(:,:,kkk,kkp);
                u=UMat_TrainSet(:,:,kkk,kkp);
                x0=XMat_TrainSet(:,:,kkk,kkp);
                
                Yvec=vec(Y);
                cvx_solver sdpt3 %Gurobi
                cvx_precision medium
                cvx_begin quiet
                
                variable xhatrob( n )
                variable dhatrob( m , T)
                ehatrob=Yvec-O*xhatrob-J*(vec(u)+vec(dhatrob));
                obj=norm(ehatrob,1)+rho*norm(xhatrob,1)+(rho+lambda)*norm(dhatrob,1);
                minimize( obj ) % Unconstrained minimization
                
                cvx_end
                cvx_clear
                
                error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
                e_rob=[e_rob; error_rob];
                
            end
            
            median_error_rob(kka,kks,kkp) = median(e_rob);   % (qactuators,qsensors)
            mean_error_rob(kka,kks,kkp) = mean(e_rob);
            max_error_rob(kka,kks,kkp) = max(e_rob);
            min_error_rob(kka,kks,kkp) = min(e_rob);
            p25_error_rob(kka,kks,kkp) = prctile(e_rob,25);
            p75_error_rob(kka,kks,kkp) = prctile(e_rob,75);
            
        end
    end
end

tElapsed = toc(tStart);

name = {'l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
save(str,'tElapsed','rhoVec','lambdaVec','lr','median_error','median_error_rob','mean_error','mean_error_rob','min_error','min_error_rob','max_error','max_error_rob','p25_error','p25_error_rob','p75_error','p75_error_rob')
display('Attack+Uncertainty')
