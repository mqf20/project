function [ case_data ] = conversion( mpc )
  % Convert data from MATPOWER's case studies format into one that is
  % usable by local code
    
  %% Network parameters
  
  case_data.num_bus = size(mpc.bus, 1);
  
  case_data.baseMVA = mpc.baseMVA;
  
  % Find generators
  
  generators = mpc.gen(:,1);  
  case_data.generators = generators;
  
  % Adjacency matrix
  
  A = zeros(case_data.num_bus);

  for branch = 1 : size(mpc.branch, 1)    
    A(mpc.branch(branch, 1), mpc.branch(branch, 2)) = 1;
    A(mpc.branch(branch, 2), mpc.branch(branch, 1)) = 1;       
  end  %for
  
  case_data.A = A;
  
  case_data.num_lines = nnz(triu(case_data.A));
  
  % Line series primitive impedance = line series resistance + j * line series reactance
  % Line series primitive impedance matrix
  
  R = zeros(case_data.num_bus);
  X = zeros(case_data.num_bus);

  for branch = 1 : case_data.num_lines 
    R(mpc.branch(branch, 1), mpc.branch(branch, 2)) = mpc.branch(branch, 3);
    R(mpc.branch(branch, 2), mpc.branch(branch, 1)) = mpc.branch(branch, 3);
    X(mpc.branch(branch, 1), mpc.branch(branch, 2)) = mpc.branch(branch, 4);
    X(mpc.branch(branch, 2), mpc.branch(branch, 1)) = mpc.branch(branch, 4);    
  end %for
  
  case_data.R = R;
  case_data.X = X;

  % Element-wise division to obtain the line series admittance matrix
  y = 1./(complex(case_data.R,case_data.X));
  
  % Line charging susceptance matrix and bus shunt susceptance (combined)
  b = zeros(case_data.num_bus);
  b(mpc.branch(branch, 1), mpc.branch(branch, 2)) = mpc.branch(branch, 5);
  b(mpc.branch(branch, 2), mpc.branch(branch, 1)) = mpc.branch(branch, 5);
    
%   b = b + diag(mpc.bus(:,6));  % Bus shunt susceptance
  
  case_data.b = b;
   
  % Extracting the real and imaginary parts of the bus admittance matrix 
  [ case_data.G, case_data.B ] = eval_admittance( case_data.num_bus, y, case_data.b, case_data.A );
  
  %% Demands for real and reactive power
  % 
  % Measured in MW and MVAR
  % Converted to p.u.

  % Demand for real power (in Per Unit)
  % Demand for reactive power (in Per Unit)
  
  P_D = zeros(case_data.num_bus, 1);
  Q_D = zeros(case_data.num_bus, 1);
  
  for bus = 1 : case_data.num_bus
    P_D(bus) = mpc.bus(bus,3);
    Q_D(bus) = mpc.bus(bus,4);
  end  %for
  
  case_data.P_D = P_D/case_data.baseMVA;  
  case_data.Q_D = Q_D/case_data.baseMVA;
    
end  %function