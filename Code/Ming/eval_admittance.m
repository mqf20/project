function [ G, B ] = eval_admittance( num_bus, y, b, A )
  %  Generates the real and imaginary parts of the bus admittance matrix

  Y = zeros(num_bus,num_bus);   
  for row = 1 : num_bus  
    for col = 1 : num_bus    
      if row == col % Diagonal entries
      	Y(row,col) = 1j * b(row,col);
        for adjacent_bus = 1 : num_bus        
          if A(row,adjacent_bus) % If a connection exists
            Y(row,col) = Y(row,col) + y(row,adjacent_bus) + 0.5j * b(row,adjacent_bus);          
          end %if
        end %for      
      else % Off-diagonal entries
        if A(row,col)        
          Y(row,col) = -y(row,col);        
        end %if      
      end %if    
    end %for  
  end %for
  
  G = real(Y);
  B = imag(Y);

end %function

