% Project l1/l2 (robust)
close all; clear all; clc;

%% Set up parameters

load ../../IEEE14bus_DataSets.mat

nSet=19;

lambdaVec =  logspace(-4,2,20);
rhoVec = linspace(0.01,1,20);%0.01:0.05:0.05*20;
lr=inf;%inf; % 2 and inf, crashes occasionally when I use lr=1

nTrain=0.5*nData;

nRho=length(rhoVec);
nLambda=length(lambdaVec);
rLambda=length(lambdaVec)/2+1:length(lambdaVec);

median_error = zeros(nLambda,1,nSets);   % (qactuators,qsensors)
mean_error = zeros(nLambda,1,nSets);
max_error = zeros(nLambda,1,nSets);
min_error = zeros(nLambda,1,nSets);
p25_error = zeros(nLambda,1,nSets);
p75_error = zeros(nLambda,1,nSets);

median_error_rob = zeros(nLambda,nRho,nSets);   % (qactuators,qsensors)
mean_error_rob = zeros(nLambda,nRho,nSets);
max_error_rob = zeros(nLambda,nRho,nSets);
min_error_rob = zeros(nLambda,nRho,nSets);
p25_error_rob = zeros(nLambda,nRho,nSets);
p75_error_rob = zeros(nLambda,nRho,nSets);

tStart = tic;

for kkp=nSet
    for kka=rLambda%1:nLambda,
        
        lambda = lambdaVec(kka);
        e_nom=[];
        %     display(kka)
        
        for kkk=1:nTrain
            %         display(kkk)
            Y=YMat_TrainSet(:,:,kkk,kkp);
            u=UMat_TrainSet(:,:,kkk,kkp);
            x0=XMat_TrainSet(:,:,kkk,kkp);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            
            cvx_solver sdpt3 %Gurobi
            cvx_precision medium
            cvx_begin quiet
            
            variable xhatnom( n )
            variable Dhatnom( m , T)
            Phi=[];
            Theta_D=[];
            for i=1:T
                Phi=[Phi C*A^(i-1)*xhatnom];
                sum_tmp=zeros(n,1);
                for j=0:i-2
                    tmp2=i-2-j;
                    if tmp2>=0
                        sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1));
                    end
                end
                Theta_D=[Theta_D D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
            end
            Ehat=Y-Phi-Theta_D;
            obj=0;%rho*norm(xhatnom,1);
            for i=1:p
                obj=obj+norm(Ehat(i,:),lr);
            end
            for i=1:m
                obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
            end
            % obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
            minimize( obj ) % Unconstrained minimization
            
            cvx_end
            cvx_clear
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom=[e_nom; error_nom];
            
        end
        median_error(kka,1,kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error(kka,1,kkp) = mean(e_nom);
        max_error(kka,1,kkp) = max(e_nom);
        min_error(kka,1,kkp) = min(e_nom);
        p25_error(kka,1,kkp) = prctile(e_nom,25);
        p75_error(kka,1,kkp) = prctile(e_nom,75);
        
        for kks=1:nRho,
            
            %         display(kks)
            rho = rhoVec(kks);
            e_rob=[];
            %         display(lambda)
            %         display(rho)
            
            for kkk=1:nTrain
                
                %             display(kkk)
                % l1/lr robust estimator with rho
                Y=YMat_TrainSet(:,:,kkk,kkp);
                u=UMat_TrainSet(:,:,kkk,kkp);
                x0=XMat_TrainSet(:,:,kkk,kkp);
                
                cvx_solver sdpt3 %Gurobi
                cvx_precision medium
                cvx_begin quiet
                
                variable xhatrob( n )
                variable Dhatrob( m , T)
                Phi=[];
                Theta_D=[];
                for i=1:T
                    Phi=[Phi C*A^(i-1)*xhatrob];
                    sum_tmp=zeros(n,1);
                    for j=0:i-2
                        tmp2=i-2-j;
                        if tmp2>=0
                            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatrob(:,j+1));
                        end
                    end
                    Theta_D=[Theta_D D*(u(:,i)+Dhatrob(:,i))+C*sum_tmp];
                end
                Ehatrob=Y-Phi-Theta_D;
                obj=rho*norm(xhatrob,1);
                for i=1:p
                    obj=obj+norm(Ehatrob(i,:),lr);
                end
                for i=1:m
                    obj=obj+rho*norm(Dhatrob(i,:),1)+lambda*norm(Dhatrob(i,:),lr);
                end
                minimize( obj ) % Unconstrained minimization
                
                cvx_end
                cvx_clear
                
                error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
                e_rob=[e_rob; error_rob];
                
            end
            
            median_error_rob(kka,kks,kkp) = median(e_rob);   % (qactuators,qsensors)
            mean_error_rob(kka,kks,kkp) = mean(e_rob);
            max_error_rob(kka,kks,kkp) = max(e_rob);
            min_error_rob(kka,kks,kkp) = min(e_rob);
            p25_error_rob(kka,kks,kkp) = prctile(e_rob,25);
            p75_error_rob(kka,kks,kkp) = prctile(e_rob,75);
            
        end
    end
end

tElapsed = toc(tStart);

name = {'part2_l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
save(str,'tElapsed','rhoVec','lambdaVec','lr','median_error','median_error_rob','mean_error','mean_error_rob','min_error','min_error_rob','max_error','max_error_rob','p25_error','p25_error_rob','p75_error','p75_error_rob')
display('Attack+Uncertainty')

