
function xhatrob=Estimate_Rob_l1(Y,u,O,J,n,m,p,T,lambda,rho)

Yvec=vec(Y);
cvx_solver sdpt3 %Gurobi
cvx_precision medium
cvx_begin quiet

variable xhatrob( n )
variable dhatrob( m , T)
ehatrob=Yvec-O*xhatrob-J*(vec(u)+vec(dhatrob));
obj=norm(ehatrob,1)+rho*norm(xhatrob,1)+(rho+lambda)*norm(dhatrob,1);
minimize( obj ) % Unconstrained minimization

cvx_end
cvx_clear

end