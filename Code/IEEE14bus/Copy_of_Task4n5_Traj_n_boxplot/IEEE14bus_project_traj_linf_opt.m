% Project l1/l2 traj

close all; clear all; clc;
Iter=500;
count=0;
avg=0;
avg_nom=0;

load Atilde.mat
load Ctilde.mat

Ts = 0.05;
n = size(Atilde,1);
Btilde = [zeros(n/2,n/2); eye(n/2,n/2)]; m = size(Btilde,2);
C = Ctilde; p = size(C,1);
Mexp=expm([Atilde Btilde; zeros(m,n+m)]*Ts);
A=Mexp(1:n,1:n);
B=Mexp(1:n,n+1:end);
D = zeros(p,m);

rho1 = 0.035;
rho2 = 0.35;
lambdanom =0.1438;%0.15; % For l2, lambda=[5,15] (10), % For l_inf, lambda=[0.05,1] (0.5)
lambdarob =1e-4;
lr=inf; % 2 and inf, crashes occasionally when I use lr=1

% n = 20;
% m = 5;
% p = 20; %15
T = 1.5*n;%15; % Number of time-steps
q_s = 1;%5;
q_a = 3;%1;
% max_qa = 5;
% max_qs = 10;

e_rob=[];
e_rob2=[];
e_nom=[];

iter=1;

while iter <=  Iter %
% for iter=1:Iter
    
    

x0=randn(n,1);

rm=randperm(m);
ra=rm(1:m-q_a);
G=eye(m);
G(:,ra)=[];

rp=randperm(p-1);
rs=rp(1:p-1-q_s);
H=eye(p);
H(:,rs)=[];
H(:,end)=[]; % the last measurement (the rotor angle) cannot be attacked


% A = 0.1.*randn(n,n); B = randn(n,m);
% C = randn(p,n); D = randn(p,m);
dA = [zeros(n/2,n); 0.01*randn(n/2,n/2), zeros(n/2,n/2)]; 
dB = [zeros(n/2,m);diag(0.1*randn(m,1))];
dC = [0.1*randn(p,n/2), zeros(p,n/2)]; dD = 0;%0.1*randn(p,m);
C_tilde=C+dC; D_tilde =D+dD;

Mexp_tilde=expm([Atilde+dA Btilde+dB; zeros(m,n+m)]*Ts);
A_tilde=Mexp_tilde(1:n,1:n);
B_tilde=Mexp_tilde(1:n,n+1:end);

display(iter)
if ~(all(abs(eig(A))<=1+1e-10) && all(abs(eig(A_tilde))<1+1e-10))
%     iter=iter+1;
%     display(iter)
%     display('accept')
% else
    display('reject')
    continue
else
    iter=iter+1;
end

Q=[eps*eye(n/2),zeros(n/2,n/2);zeros(n/2,n/2), 1e-1*eye(n/2)];%diag([0.1 0.16 0.2 0.12 0.25 0.14 0.03 0.21 0.3 0.02 0.09 0.18]);
R=1e-1*eye(p);%*diag([0.21 0.06 0.22 0.02 0.19 0.14 0.13 0.11 0.23 0.12 0.03 0.18]);

% T time steps
k=1:T;
attackpower = 0.01;
d=attackpower*randn(q_a,length(k));
e=attackpower*randn(q_s,length(k));
u=0.01*randn(m,length(k));

% Generate w and v
SigmaQ = chol(Q);
w = (randn(length(k),n)*SigmaQ)';
SigmaR = chol(R);
v = (randn(length(k),p)*SigmaR)';

% Dynamics
x=zeros(n,length(k));
y=zeros(p,length(k));
x(:,1)=x0;
for i=k(1:k(end))
    y(:,i)=C_tilde*x(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
    if i<k(end)
        x(:,i+1)=A_tilde*x(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
    end
%     % no uncertainties
%     y(:,i)=C*x(:,i)+D*G*d(:,i)+H*e(:,i);
%     if i<k(end)
%         x(:,i+1)=A*x(:,i)+B*G*d(:,i);
%     end
end
Y=y;

cvx_solver Gurobi
cvx_precision medium
cvx_begin quiet
variable xhat( n ) 
variable Dhat( m , T)
Phi=[];
Theta=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhat];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat(:,j+1)); 
       end
    end
    Theta=[Theta D*(u(:,i)+Dhat(:,i))+C*sum_tmp];
end
Ehat=Y-Phi-Theta;
obj=rho1*norm(xhat,1);
for i=1:p
    obj=obj+norm(Ehat(i,:),lr);
end
for i=1:m
    obj=obj+rho1*norm(Dhat(i,:),1)+lambdarob*norm(Dhat(i,:),lr);
end
% obj
% sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2))
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2));
minimize( obj )

cvx_end

cvx_clear


cvx_solver Gurobi
cvx_precision medium
cvx_begin quiet
variable xhat2( n ) 
variable Dhat2( m , T)
Phi=[];
Theta=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhat2];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat2(:,j+1)); 
       end
    end
    Theta=[Theta D*(u(:,i)+Dhat2(:,i))+C*sum_tmp];
end
Ehat2=Y-Phi-Theta;
obj=rho2*norm(xhat2,1);
for i=1:p
    obj=obj+norm(Ehat2(i,:),lr);
end
for i=1:m
    obj=obj+rho2*norm(Dhat2(i,:),1)+lambdarob*norm(Dhat2(i,:),lr);
end
% obj
% sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2))
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2));
minimize( obj )

cvx_end

cvx_clear

% Nominal
%rho=0;
cvx_solver Gurobi
cvx_precision medium
cvx_begin quiet
variable xhatnom( n ) 
variable Dhatnom( m , T)
Phi=[];
Theta=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhatnom];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1)); 
       end
    end
    Theta=[Theta D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
end
Ehat=Y-Phi-Theta;
obj=0;%rho*norm(xhatnom,1);
for i=1:p
    obj=obj+norm(Ehat(i,:),lr);
end
for i=1:m
    obj=obj+lambdanom*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
end
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
minimize( obj )

cvx_end

cvx_clear

xnom=zeros(n,length(k));
xnom(:,1)=xhatnom;
for i=k(1:k(end))
    if i<k(end)
        xnom(:,i+1)=A*xnom(:,i)+B*(u(:,i)+Dhatnom(:,i));%+w(:,i);
    end

end

xrob=zeros(n,length(k));
xrob(:,1)=xhat;
for i=k(1:k(end))
    if i<k(end)
        xrob(:,i+1)=A*xrob(:,i)+B*(u(:,i)+Dhat(:,i));%+w(:,i);
    end

end

xrob2=zeros(n,length(k));
xrob2(:,1)=xhat2;
for i=k(1:k(end))
    if i<k(end)
        xrob2(:,i+1)=A*xrob2(:,i)+B*(u(:,i)+Dhat2(:,i));%+w(:,i);
    end

end

err_rob=norms(x-xrob,1)./norms(x,1);
err_rob2=norms(x-xrob2,1)./norms(x,1);
err_nom=norms(x-xnom,1)./norms(x,1);
% plot(k,err_nom,k,err_rob,k,err_rob2)

e_rob=[e_rob; err_rob];
e_rob2=[e_rob2; err_rob2];
e_nom=[e_nom; err_nom];


end

figure
boxplot([e_rob(:,1),e_rob2(:,1),e_nom(:,1)],'whisker',1000)

median_error_nom = median(e_nom);   
mean_error_nom = mean(e_nom);
std_error_nom = std(e_nom);
max_error_nom = max(e_nom);
min_error_nom = min(e_nom);
p25_error_nom = prctile(e_nom,25);
p75_error_nom = prctile(e_nom,75);

median_error_rob = median(e_rob);   
mean_error_rob = mean(e_rob);
std_error_rob = std(e_rob);
max_error_rob = max(e_rob);
min_error_rob = min(e_rob);
p25_error_rob = prctile(e_rob,25);
p75_error_rob = prctile(e_rob,75);

median_error_rob2 = median(e_rob2);   
mean_error_rob2 = mean(e_rob2);
std_error_rob2 = std(e_rob2);
max_error_rob2 = max(e_rob2);
min_error_rob2 = min(e_rob2);
p25_error_rob2 = prctile(e_rob2,25);
p75_error_rob2 = prctile(e_rob2,75);
% avg=avg/Iter
% avg_nom=avg_nom/Iter

figure
subplot(4,1,1)
errorbar(k,median_error_nom,p25_error_nom,p75_error_nom,'k-')
hold all;
errorbar(k,median_error_rob,p25_error_rob,p75_error_rob,'b--')
errorbar(k,median_error_rob2,p25_error_rob2,p75_error_rob2,'r-.')
ylabel('median with p25 and p75')
subplot(4,1,2)
errorbar(k,median_error_nom,min_error_nom,max_error_nom,'k-')
hold all;
errorbar(k,median_error_rob,min_error_rob,max_error_rob,'b--')
errorbar(k,median_error_rob2,min_error_rob2,max_error_rob2,'r-.')
ylabel('median with min and max')
subplot(4,1,3)
errorbar(k,mean_error_nom,std_error_nom,'k-')
hold all;
errorbar(k,mean_error_rob,std_error_rob,'b--')
errorbar(k,mean_error_rob2,std_error_rob2,'r-.')
ylabel('mean with std')
subplot(4,1,4)
errorbar(k,mean_error_nom,min_error_nom,max_error_nom,'k-')
hold all;
errorbar(k,mean_error_rob,min_error_rob,max_error_rob,'b--')
errorbar(k,mean_error_rob2,min_error_rob2,max_error_rob2,'r-.')
ylabel('mean with min and max')

save('l1_linf_traj_q13_both_opt.mat','e_rob','e_rob2','e_nom','k')
sendmail('szyong@mit.edu','Task1_both done!','Simulation done.');

display('Attack+Uncertainty')
