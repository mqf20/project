\documentclass[11pt]{beamer}
\usetheme{CambridgeUS}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\author{Sze Zheng Yong (\texttt{szyong@mit.edu}) \\ Ming Qing Foo (\texttt{mqf20@mit.edu})}
\title{Robust Estimation for Cyber-Physical Systems Under Adversarial Attacks}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
%\date{} 
%\subject{} 

\newcommand{\argmin}{\operatornamewithlimits{arg\ min}}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

%\begin{frame}
%\tableofcontents
%\end{frame}

%\begin{frame}{Motivation}
%\end{frame}
%
%\begin{frame}{Literature Review}
%\end{frame}

\section{Problem Formulation}

\begin{frame}
\sectionpage
\end{frame}

\begin{frame}{Dynamical System}

We can model a CPS as a LTI dynamical system 

\begin{align*} 
 \begin{array}{rl}
  x_{k+1} &= \widetilde{A} \ x_{k} + \widetilde{B} \ (u_k + d_k) + w_k\\
  y_{k} &= \widetilde{C} \ x_{k} + \widetilde{D} \ (u_k + d_k) + e_{k} +v_k
  \end{array}
\end{align*}

\begin{footnotesize}
\begin{itemize}
\item Uncertain system parameters: $\widetilde{A}$, $\widetilde{B}$, $\widetilde{C}$, $\widetilde{D}$
\item State: $x_{k}$
\item Known input: $u_{k}$
\item Observed output: $y_{k}$
\item Noises: $w_{k}$, $v_{k}$
\item Attacks: $d_{k}$ (actuators), $e_{k}$ (sensors)
\end{itemize}
\end{footnotesize}

\end{frame}

\begin{frame}{Attacker Model}

The attacker has the following capabilities:
\begin{itemize}
\item Can inject false data into subset of sensors and actuators - these sensors and actuators are said to be ``compromised''
\item Attacks can have arbitrary magnitudes
\end{itemize}

\end{frame}

\begin{frame}{Objective}

Given the observations $y_{0}, \cdots, y_{T-1}$, we want to
\begin{itemize}
\item Estimate the sequence of states $x_{0}, \cdots, x_{T-1}$
\item Robust to modeling errors in system parameters: $\widetilde{A}$, $\widetilde{B}$, $\widetilde{C}$, $\widetilde{D}$
\end{itemize}

\end{frame}

\section{Robust Estimation}

\begin{frame}
\sectionpage
\end{frame}

\begin{frame}{Overview}

Procedure:
\begin{itemize}
\item Design a robust estimator to obtain a robust estimate of the initial state $x_{0}$ and actuator attacks $d_{0}, \cdots, d_{T-1}$
\item Propagate forward
\end{itemize}

\end{frame}

\begin{frame}{Nominal Estimator}

In the absence of uncertainties, the nominal estimator is given by

\begin{equation*}
( {x}_{0}, {\mathrm{D}} ) \ = \ \argmin_{\substack{x_{0} \in \mathbb{R}^{n} \\\mathrm{D} \in \mathbb{R}^{m \times T}}} \ \left\| \mathrm{Y} - \Phi(x_{0}) - \Theta (\mathrm{D}) \right\|_{\ell_0} + \left\| \mathrm{D} \right\|_{\ell_0}
\end{equation*}

\begin{itemize}
\item $\mathrm{Y} := \begin{bmatrix} y_{0}, \cdots, y_{T-1} \end{bmatrix}$, $\mathrm{D} := \begin{bmatrix} d_{0}, \cdots, d_{T-1} \end{bmatrix}$
\item $\Phi ( \cdot )$ and $\Theta ( \cdot )$ are linear maps
\item $\| M \|_{\ell_0} = $number of non-zero rows of $M$. 
\item Minimizes the number of compromised sensors and actuators
\item The rest of the states $x_{1}, \cdots, x_{T-1}$ can be obtained using known system dynamics
\end{itemize}

However,
\begin{itemize}
\item ``$\ell_0$'' norm is NP-hard to compute
\item Optimal estimator is intractable
\end{itemize}

\end{frame}

\begin{frame}{Nominal Estimator - Relaxed}

Relaxation using a mixed ``$\ell_1 / \ell_r$'' norm

\begin{equation}
( \hat{x}_{0}, \hat{\mathrm{D}} ) \ = \ \argmin_{\substack{x_{0} \in \mathbb{R}^{n} \\\mathrm{D} \in \mathbb{R}^{m \times T}}} \ \left\| \mathrm{Y} - \Phi(x_{0}) - \Theta (\mathrm{D}) \right\|_{\ell_1/\ell_r} +\lambda \left\| \mathrm{D} \right\|_{\ell_1/\ell_r}
\end{equation}

\begin{itemize}
\item ``Mixed norm'': $\| \mathrm{M} \|_{\ell_1 / \ell_r} = \sum_{i} \| \mathrm{E}_{(i,\cdot)} \|_{\ell_r}$
\item $\lambda$ is a tuning parameter
\end{itemize}

\end{frame}

\begin{frame}{Robust Estimator - First Attempt}

\begin{footnotesize}
\begin{align*}
  ( \hat{x}_{0}, \hat{\mathrm{D}} ) = \argmin_{\substack{x_{0} \in \mathbb{R}^{n} \\\mathrm{D} \in \mathbb{R}^{m \times T}}} \ \max_{ \delta \Psi  \in \mathcal{U}} \ \left\| \mathrm{Y} - \widetilde{\Phi} ( x_{0} ) - \widetilde{\Theta} (\mathrm{U}) -\widetilde{\Theta} (\mathrm{D}) - \widetilde{\Upsilon} (\mathrm{W},\mathrm{V}) \right\|_{\ell_0} + \|\mathrm{D}\|_{\ell_0}
\end{align*}
\end{footnotesize}

\begin{itemize}
\item $\mathrm{U} := \begin{bmatrix} y_{0}, \cdots, y_{T-1} \end{bmatrix}$, same for $\mathrm{U}$, $\mathrm{D}$, $\mathrm{W}$ and $\mathrm{V}$
\item $\widetilde{\Phi} ( \cdot )$, $\widetilde{\Theta} ( \cdot )$ and $\widetilde{\Upsilon} ( \cdot )$ are linear maps
\item Uncertain variable $\delta \Psi$ and uncertainty set $\mathcal{U}$ take into account noises and uncertainties (explained later)
\end{itemize}

Evaluation:

\begin{itemize}
\item Makes sense? \textbf{No}
\item Tractable? \textbf{No}
\end{itemize}

\end{frame}

\begin{frame}{Robust Estimator - Second Attempt}

Using a ``$\ell_1 / \ell_r$'' relaxation,

\begin{footnotesize}
\begin{align*}
  ( \hat{x}_{0}, \hat{\mathrm{D}} ) = \argmin_{\substack{x_{0} \in \mathbb{R}^{n} \\\mathrm{D} \in \mathbb{R}^{m \times T}}} \ \max_{ \delta \Psi  \in \mathcal{U}} \ \left\| \mathrm{Y} - \widetilde{\Phi} ( x_{0} ) - \widetilde{\Theta} (\mathrm{U}) -\widetilde{\Theta} (\mathrm{D}) - \widetilde{\Upsilon} (\mathrm{W},\mathrm{V}) \right\|_{\ell_1/\ell_r} + \lambda \|\mathrm{D}\|_{\ell_1/\ell_r}
\end{align*}
\end{footnotesize}

Evaluation:

\begin{itemize}
\item Makes sense? \textbf{Yes}
\item Tractable? \textbf{Yes}, for a suitable choice of uncertainty set $\mathcal{U}$ (explained next)
\end{itemize}

\end{frame}

\begin{frame}{Equivalence of Robust Regression and $\ell_q$-Regularization}

\begin{theorem}

Let $\Psi$ be an uncertain matrix belonging to the uncertainty set $\mathcal{U}_{(\ell_{q},\ell_{r})}=\{\delta\Psi: \|\delta\Psi\|_{(\ell_q,\ell_r)} \leq \rho \}$\footnote{$( \ell_q, \ell_r )$ subordinate norm $\| M \|_{( \ell_q, \ell_r )} := \max_{\boldsymbol\beta \neq 0} \frac{\| M \, \boldsymbol\beta\|_{\ell_r}}{\|\boldsymbol\beta\|_{\ell_q}}$}. If $q,r \in [1,\infty]$ then for any vector $\boldsymbol\beta$, we have
\begin{align*}
 \max_{\delta\Psi \in \mathcal{U}_{(\ell_{q},\ell_{r})}} \|\mathbf{y}-(\Psi +\delta\Psi) \boldsymbol\beta \|_{\ell_r}= \|\mathbf{y}-\Psi \boldsymbol\beta \|_{\ell_r}+ \rho \|\boldsymbol\beta\|_{\ell_q}.
\end{align*}

\end{theorem}

\end{frame}

\begin{frame}{Robust Estimator}

Choose the uncertainty set
\[
  \mathcal{U}_{(\ell_{q},\ell_{r})}=\{\delta\Psi: \|\delta\Psi\|_{(\ell_q,\ell_r)} \leq \rho \}
\]

and choose $\ell_{q} = \ell_{1}$.

\vspace{0.5cm}

Our robust estimator is
\begin{footnotesize}
\begin{align*}
  \begin{array}{rl}
  \displaystyle (\hat{x}_{0},\hat{\mathrm{D}}) = \argmin_{\substack{x_{0} \in \mathbb{R}^{n} \\\mathrm{D} \in \mathbb{R}^{m \times T}}}  \left\| \mathrm{Y} - \Phi ( x_{0} ) - \Theta (\mathrm{D}) - \Theta (\mathrm{U}) \right\|_{\ell_1/\ell_r} + \rho \| x_{0} \|_{\ell_1} + \rho \| \mathrm{D} \|_{\ell_1/\ell_1} + \lambda \|\mathrm{D}\|_{\ell_1/\ell_r}
  \end{array}
\end{align*}
\end{footnotesize}

\begin{itemize}
\item $\rho$ is a parameter that controls the amount of robustification
\item $\lambda$ is a parameter that weighs the importance of canceling out attacks on the actuators vs sensors
\item Use statistical learning to determine values of $\rho$ and $\lambda$
\end{itemize}

\end{frame}

\begin{frame}{Forward Propagation}

We now have the robust estimates $(\hat{x}_{0},\hat{\mathrm{D}})$. but we cannot use the dynamics 
\[
  x_{k+1} = \widetilde{A} \ x_{k} + \widetilde{B} \ (u_k + d_k) + w_k
\]

to obtain estimates of $x_{1}, \cdots, x_{T-1}$ because we do not know $\widetilde{A}$, $\widetilde{B}$ and $w_{k}$.

\vspace{0.5cm}

Solution: use robust estimation

Rewrite the 

\end{frame}

\end{document}