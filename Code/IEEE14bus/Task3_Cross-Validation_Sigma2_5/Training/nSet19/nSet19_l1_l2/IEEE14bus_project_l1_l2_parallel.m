% Project l1/l2 (robust)
close all; clear all; clc;

%% Set up parameters

load ../../IEEE14bus_DataSets.mat

myCluster=parcluster('local'); 
myCluster.NumWorkers=2; parpool(myCluster,2);

nSet=19;

lambdaVec =  logspace(-4,2,20);
rhoVec = linspace(0.01,1,20);%0.01:0.05:0.05*20;
lr=2;%inf; % 2 and inf, crashes occasionally when I use lr=1

nTrain=0.5*nData;

nRho=length(rhoVec);
nLambda=length(lambdaVec);

median_error = zeros(nLambda,1,nSets);   % (qactuators,qsensors)
mean_error = zeros(nLambda,1,nSets);
max_error = zeros(nLambda,1,nSets);
min_error = zeros(nLambda,1,nSets);
p25_error = zeros(nLambda,1,nSets);
p75_error = zeros(nLambda,1,nSets);

median_error_rob = zeros(nLambda,nRho,nSets);   % (qactuators,qsensors)
mean_error_rob = zeros(nLambda,nRho,nSets);
max_error_rob = zeros(nLambda,nRho,nSets);
min_error_rob = zeros(nLambda,nRho,nSets);
p25_error_rob = zeros(nLambda,nRho,nSets);
p75_error_rob = zeros(nLambda,nRho,nSets);

tStart = tic;

for kkp=nSet
    for kka=1:nLambda,
        
        lambda = lambdaVec(kka);
        e_nom=zeros(1,nTrain);
        %     display(kka)
        
        parfor kkk=1:nTrain
            %         display(kkk)
            Y=YMat_TrainSet(:,:,kkk,kkp);
            u=UMat_TrainSet(:,:,kkk,kkp);
            x0=XMat_TrainSet(:,:,kkk,kkp);
            
            % Nominal estimator
            %  Same as the robust estimator but with rho=0;
            xhatnom=Estimate_Nom(Y,u,A,B,C,D,n,m,p,T,lr,lambda);
            
            error_nom=norm((xhatnom-x0))/(norm(x0)+eps);
            e_nom(1,kkk)=error_nom;
            
        end
        median_error(kka,1,kkp) = median(e_nom);   % (qactuators,qsensors)
        mean_error(kka,1,kkp) = mean(e_nom);
        max_error(kka,1,kkp) = max(e_nom);
        min_error(kka,1,kkp) = min(e_nom);
        p25_error(kka,1,kkp) = prctile(e_nom,25);
        p75_error(kka,1,kkp) = prctile(e_nom,75);
        
        for kks=1:nRho,
            
            %         display(kks)
            rho = rhoVec(kks);
            e_rob=zeros(1,nTrain);
            %         display(lambda)
            %         display(rho)
            
            parfor kkk=1:nTrain
                
                %             display(kkk)
                % l1/lr robust estimator with rho
                Y=YMat_TrainSet(:,:,kkk,kkp);
                u=UMat_TrainSet(:,:,kkk,kkp);
                x0=XMat_TrainSet(:,:,kkk,kkp);
                
                xhatrob=Estimate_Rob(Y,u,A,B,C,D,n,m,p,T,lr,lambda,rho);
                
                error_rob=norm((xhatrob-x0))/(norm(x0)+eps);
                e_rob(1,kkk)= error_rob;
                
            end
            
            median_error_rob(kka,kks,kkp) = median(e_rob);   % (qactuators,qsensors)
            mean_error_rob(kka,kks,kkp) = mean(e_rob);
            max_error_rob(kka,kks,kkp) = max(e_rob);
            min_error_rob(kka,kks,kkp) = min(e_rob);
            p25_error_rob(kka,kks,kkp) = prctile(e_rob,25);
            p75_error_rob(kka,kks,kkp) = prctile(e_rob,75);
            
        end
    end
end

tElapsed = toc(tStart);

name = {'l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
save(str,'tElapsed','rhoVec','lambdaVec','lr','median_error','median_error_rob','mean_error','mean_error_rob','min_error','min_error_rob','max_error','max_error_rob','p25_error','p25_error_rob','p75_error','p75_error_rob')
display('Attack+Uncertainty')
delete(gcp)
sendmail('szyong@mit.edu','nSet19_l1_l2 done!','Simulation done.');

