% Plot array

close all; clear all; clc;
% load('results.mat');
load l1_lr_Array.mat

max_qa = 5; % Maximum number of actuators attacked
max_qs = 10; % Maximum number of sensors attacked
qavec = 0:max_qa;
qsvec = 0:max_qs;

set(0,'defaulttextinterpreter','latex')
figure
minc=floor(min([min(min(mean_error)),min(min(mean_error_r1)),min(min(mean_error_r2))]));
maxc=ceil(max([max(max(mean_error)),max(max(mean_error_r1)),max(max(mean_error_r2))]));
subplot(1,2,1)
imagesc(qsvec,qavec,mean_error);
set(gca,'YDir','normal');
colormap(flipud(gray)); colorbar;
title({'\quad Normalized mean error of nominal $\ell_1/\ell_\infty$ decoder', ... % ($\rho=0$)
    'as function of number of attacked sensors and actuators'});
xlabel('Attacked sensors');
ylabel('Attacked actuators');
caxis([minc,maxc])

subplot(1,2,2)
imagesc(qsvec,qavec,mean_error_r1);
set(gca,'YDir','normal');
colormap(flipud(gray)); colorbar;
title({'','\quad \ Normalized mean error of robust $\ell_1/\ell_\infty$ decoder', ... % ($\rho=0.1$) 
    'as function of number of attacked sensors and actuators'});
xlabel('Attacked sensors');
ylabel('Attacked actuators');
caxis([minc,maxc])

% subplot(1,3,3)
% imagesc(qsvec,qavec,mean_error_r2);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Normalized mean error of robust $\ell_1/\ell_\infty$ decoder ($\rho=1$)', ...
%     '\ as function of number of attacked sensors and actuators'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])

figure
minc=floor(min([min(min(median_error)),min(min(median_error_r1)),min(min(median_error_r2))]));
maxc=ceil(max([max(max(median_error)),max(max(median_error_r1)),max(max(median_error_r2))]));
subplot(1,2,1)
imagesc(qsvec,qavec,median_error);
set(gca,'YDir','normal');
colormap(flipud(gray)); colorbar;
title({'\quad \ Normalized median error of nominal $\ell_1/\ell_\infty$ decoder', ... % ($\rho=0$)
    'as function of number of attacked sensors and actuators'});xlabel('Attacked sensors');
ylabel('Attacked actuators');
caxis([minc,maxc])

%figure;
subplot(1,2,2)
imagesc(qsvec,qavec,median_error_r1);
set(gca,'YDir','normal');
colormap(flipud(gray)); colorbar;
title({'','\quad Normalized median error of robust $\ell_1/\ell_\infty$ decoder', ... % ($\rho=0.1$)
    'as function of number of attacked sensors and actuators'});
xlabel('Attacked sensors');
ylabel('Attacked actuators');
caxis([minc,maxc])

%figure;
% subplot(1,3,3)
% imagesc(qsvec,qavec,median_error_r2);
% set(gca,'YDir','normal');
% colormap(flipud(gray)); colorbar;
% title({'Normalized median error of robust $\ell_1/\ell_\infty$ decoder ($\rho=1$)', ...
%     '\ as function of number of attacked sensors and actuators'});
% xlabel('Attacked sensors');
% ylabel('Attacked actuators');
% caxis([minc,maxc])

% rho
% for i = 1 : size(r,2)
%   % Parse r
%   optval(i) = r{i}.optval;
%   Gamma(i) = r{i}.Gamma;
%   % Generate violation probabilities (from Lecture 5 page 10)
%   Phi(i) = qfunc((Gamma(i)-1)/sqrt(F*C));
%   
% end  %for
% 
% 
% % figure; 
% % plot(Gamma,optval,'*'); grid on;
% % xlabel('Gamma'); ylabel('Worst-case cost');
% 
% figure;
% plot(Gamma,Phi)
% [ax, p1, p2] = plotyy(Gamma,optval,Gamma,Phi); grid on;
% p1.LineStyle = '--';
% p1.LineWidth = 2;
% p2.LineWidth = 2;
% ylabel(ax(1),'Worst-case cost') % label left y-axis
% ylabel(ax(2),'Violation probability') % label right y-axis
% xlabel(ax(1),'\Gamma') % label x-axis
% 
