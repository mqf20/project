clear all;

n = 5;
m = 4;
p = 6;
T = 10; % Number of time steps

A = randn(n,n); B = randn(n,m);
C = randn(p,n); D = randn(p,m);

x0 = randn(n,1);

noise_scaling = 0.1;

%% Inputs

d = randn(m,T);
d0 = d(:,1);
d_compact = vertcat(d(:));

u = randn(m,T);
u0 = u(:,1);
u_compact = vertcat(u(:));

e = randn(p,T);
e_compact = vertcat(e(:));

w = randn(n,T);
w_compact = vertcat(w(:));

v = randn(p,T);
v_compact = vertcat(v(:));

%% Uncertainties

Au = A + randn(size(A)) * noise_scaling;
Bu = B + randn(size(B)) * noise_scaling;
Cu = C + randn(size(C)) * noise_scaling;
Du = D + randn(size(D)) * noise_scaling;

%% Actual system

x = x0;

for t = 1 : T
  
  Y(:,t) = Cu * x + Du * ( u(:,t) + d(:,t))  + e(:,t) + v(:,t);
  x = Au * x + Bu * (u(:,t) + d(:,t) ) + w(:,t);
  
end %for

Y_compact = vertcat(Y(:));

%% Proposed system

Phi(:,1) = Cu * x0;

Theta_d(:,1) = Du * d0;

Theta_u(:,1) = Du * u0;

Gamma(:,1) = v(:,1);

Y2(:,1) = Phi(:,1) + Theta_d(:,1) + Theta_u(:,1) + v(:,1) + e(:,1);

errors(:,1) = norm(Y(:,1) - Y2(:,1));

for t = 2 : T
  
  Phi(:,t)   = Cu * Au^(t-1) * x0;
  
  theta_d = zeros(p,1);
  theta_u = zeros(p,1);
  gamma = zeros(p,1);
  for s = 1 : t-1
    theta_d = theta_d + Cu * Au^(t-1-s) * Bu * d(:,s);
    theta_u = theta_u + Cu * Au^(t-1-s) * Bu * u(:,s);
    gamma = gamma + Cu * Au^(t-1-s) * w(:,s);
  end %for
    
  Theta_d(:,t) = theta_d + Du * d(:,t);
  
  Theta_u(:,t) = theta_u + Du * u(:,t);
  
  Gamma(:,t) = gamma + v(:,t);

  Y2(:,t) = Phi(:,t) + Theta_d(:,t) + Theta_u(:,t) + Gamma(:,t) + e(:,t);
  
  errors(:,t) = norm(Y(:,t) - Y2(:,t));
  
end %for

%% Plotting

% figure;
% semilogy(errors); grid on;

fprintf('Y-Y2 has norm %e\n', norm(Y-Y2));

%% Compact representation

O_compact = [];
O_tilde_compact = [];

J_u = [];
J_tilde_u = [];

J_w = [];
J_tilde_w = [];

for t = 1 : T
  
  O_compact = [O_compact; C * A ^ (t-1)];
  O_tilde_compact = [O_tilde_compact; Cu * Au ^ (t-1)];
  
  J_u_row = [];
  J_tilde_u_row = [];
  
  J_w_row = [];
  J_tilde_w_row = [];
   
  for s = 2 : t
    
    J_u_row = [J_u_row, C * A ^ (t-s) * B];  
    J_tilde_u_row = [J_tilde_u_row, Cu * Au ^ (t-s) * Bu];
    
    J_w_row = [J_w_row, C * A ^ (t-s)];
    J_tilde_w_row = [J_tilde_w_row, Cu * Au ^ (t-s)];
    
  end %for
  
  J_u_row = [J_u_row, Du, zeros(p,m*(T-t))];
  J_tilde_u_row = [J_tilde_u_row, Du, zeros(p,m*(T-t))];
  
  J_w_row = [J_w_row, zeros(p,n*(T-t+1))];
  J_tilde_w_row = [J_tilde_w_row, zeros(p,n*(T-t+1))];
  
  J_u = [J_u; J_u_row];
  J_tilde_u = [J_tilde_u; J_tilde_u_row];
  
  J_w = [J_w; J_w_row];
  J_tilde_w = [J_tilde_w; J_tilde_w_row];
  
end %for

Y2_compact = O_tilde_compact * x0 + J_tilde_u * (u_compact + d_compact) + J_tilde_w * w_compact + e_compact + v_compact;

fprintf('Y_compact - Y2_compact has norm %e\n', norm(Y_compact - Y2_compact));

%% Analysis

delta_O = O_tilde_compact - O_compact;
delta_J_u = J_tilde_u - J_u;

delta_O_plus = [];
O_plus = [];

J_u_plus = [];
% J_tilde_u_plus = [];
delta_J_u_plus = [];

for t = 1 : T
  
  delta_O_plus = [delta_O_plus, Cu * Au ^ (t-1)];
  O_plus = [O_plus, C * A ^ (t-1)];
  
  J_u_plus = [J_u_plus, J_u(1+p*(t-1):p*t, 1:m*t)];
%   J_tilde_u_plus = [J_tilde_u_plus, J_tilde_u(1+p*(t-1):p*t, 1:m*t)];
  delta_J_u_plus = [delta_J_u_plus, delta_J_u(1+p*(t-1):p*t, 1:m*t)];
  
end

