function IEEE_14_vary_sigma()

  % IEEE 14 bus example
  % Vary rho

  close all; clear all; clc;

  %% Set up parameters

  max_iter = 100; % Number of iterations (for averaging)

  % Load data
  load Atilde.mat;
  load Ctilde.mat;
  n = size(Atilde,1); % n = 10;
  p = size(Ctilde,1); % p = 35;
  Btilde = [zeros(n/2,n/2); eye(n/2,n/2)];
  m = size(Btilde,2); % m = 5;

  Ts = 0.05; % Discretization time step

  % Nominal system
  Mexp = expm([Atilde Btilde; zeros(m,n+m)]*Ts);
  A    = Mexp(1:n,1:n);
  B    = Mexp(1:n,n+1:end);
  C    = Ctilde; 
  D    = zeros(p,m); 

  sigmaVec = 0:0.1:5;
  rho1     = 0.1;
  rho2     = 1;
  lambda   = 0.2;

  T     = 1.5 * n; %15; % Number of time-steps
  numqa = 1;       % Number of attacked actuators
  numqs = 3;       % Number of attacked sensors

  %% Initialization

  template1      = struct;
  template1.rho1 = zeros(length(sigmaVec),1);
  template1.rho2 = zeros(length(sigmaVec),1);

  template2          = struct;
  template2.l1_l1    = template1;
  template2.l1_l2    = template1;
  template2.l1_linf  = template1;
  template2.nom_l1   = zeros(length(sigmaVec),1);
  template2.nom_l2   = zeros(length(sigmaVec),1);
  template2.nom_linf = zeros(length(sigmaVec),1);

  % Construct holders for our data using the templates
  median_error = template2;
  mean_error   = template2;
  std_error    = template2;
  min_error    = template2;
  max_error    = template2;
  p25_error    = template2;
  p75_error    = template2;

  clear template1 template2

  template  = struct;
  template.rho1 = zeros(max_iter,1);
  template.rho2 = zeros(max_iter,1);

  tic

  for iter1 = 1 : length(sigmaVec) % Vary values of rho

      %% Initialization

      sigma = sigmaVec(iter1);

      iter2 = 1;

      e_rob_l1   = template;
      e_rob_l2   = template;
      e_rob_linf = template;
      e_nom_l1   = zeros(max_iter,1);
      e_nom_l2   = zeros(max_iter,1);
      e_nom_linf = zeros(max_iter,1);

      fprintf('\n* iter_1 = %d\n', iter1);

      while iter2 <= max_iter % Run many iterations and get average

          %% Generate random system

          x0 = randn(n,1); % Initial state

          % Actuator attacks
          rm      = randperm(m);
          ra      = rm(1:m-numqa); % Actuators that are NOT attacked
          G       = eye(m); 
          G(:,ra) = []; % Actuator attack matrix

          % Sensor attacks
          rp       =randperm(p-1);
          rs       = rp(1:p-1-numqs); % Sensors that are NOT attacked
          H        = eye(p);
          H(:,rs)  = []; % Sensor attack matrix
          H(:,end) = []; % The last measurement (rotor angle) cannot be attacked

          % Perturbations        
          dA = [zeros(n/2,n); 0.01*sigma*randn(n/2,n/2), zeros(n/2,n/2)]; 
          dB = [zeros(n/2,m);diag(0.1*sigma*randn(m,1))];
          dC = [0.1*sigma*randn(p,n/2), zeros(p,n/2)]; 
          dD = 0;

          % Uncertain system
          Mexp_tilde = expm([Atilde+dA Btilde+dB; zeros(m,n+m)]*Ts);
          A_tilde    = Mexp_tilde(1:n,1:n);
          B_tilde    = Mexp_tilde(1:n,n+1:end);
          C_tilde    = C + dC; 
          D_tilde    = D + dD;

          fprintf('**  iter_2 = %d\n', iter2);
          if ~(all(abs(eig(A))<=1+1e-10) && all(abs(eig(A_tilde))<1+1e-10)) % If unstable
              fprintf('**  Rejected\n');
              continue
          else
              fprintf('**  Accepted\n');
              iter2=iter2+1;
          end %if

          % Noise covariance matrices
          Q = [eps*eye(n/2),zeros(n/2,n/2);zeros(n/2,n/2), eye(n/2)];
          R = eye(p);

          %% Attacks and noises

          attackpower = 1;
          d           = attackpower*randn(numqa,T); % Actuator attacks
          e           = attackpower*randn(numqs,T); % Sensor attacks

          u = 0.01*randn(m,T); % Known inputs

          % Generate noises
          if sigma == 0
              w = zeros(n,T);
              v = zeros(p,p);          
          else
              SigmaQ = chol(Q);
              w      = (1e-1*sigma*randn(T,n)*SigmaQ)';
              SigmaR = chol(R);
              v      = (1e-1*sigma*randn(T,p)*SigmaR)';            
          end %if

          %% Actual dynamics

          X=zeros(n,T);
          Y=zeros(p,T);

          X(:,1)=x0;

          for i=1:T
              Y(:,i)=C_tilde*X(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
              if i<T
                  X(:,i+1)=A_tilde*X(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
              end %if
          end %for

          %% l1/l1 robust estimator with rho1 and rho2

          lr = 1;
          
          [xhat, ~] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho1);

          e_rob_l1.rho1(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear xhat
          
          [xhat, ~] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho2);

          e_rob_l1.rho2(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear lr xhat

          %% l1/l2 robust estimator with rho1 and rho2

          lr = 2;

          [xhat, ~] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho1);

          e_rob_l2.rho1(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear xhat
          
          [xhat, ~] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho2);

          e_rob_l2.rho2(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear lr xhat

          %% l1/linf robust estimator with rho1 and rho2

          lr = inf;

          [xhat, ~] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho1);

          e_rob_linf.rho1(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear xhat
          
          [xhat, ~] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho2);

          e_rob_linf.rho2(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear lr xhat

          %% Nominal estimator with l1/l1
          %  Same as the robust estimator but with rho=0;

          lr = 1;

          [xhat, ~] = nominal_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda);

          e_nom_l1(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear lr xhat

          %% Nominal estimator with l1/l2
          %  Same as the robust estimator but with rho=0;

          lr = 2;

          [xhat, ~] = nominal_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda);

          e_nom_l2(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear lr xhat

          %% Nominal estimator with l1/linf
          %  Same as the robust estimator but with rho=0;

          lr = inf;

          [xhat, ~] = nominal_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda);

          e_nom_linf(iter2) = norm(xhat-x0)/(norm(x0)+eps);

          clear lr xhat  

      end %while

      %% Process results
      
      median_error.l1_l1.rho1(iter1) = median(e_rob_l1.rho1);     median_error.l1_l1.rho2(iter1) = median(e_rob_l1.rho2);
      mean_error.l1_l1.rho1(iter1)   = mean(e_rob_l1.rho1);       mean_error.l1_l1.rho2(iter1)   = mean(e_rob_l1.rho2);
      std_error.l1_l1.rho1(iter1)    = std(e_rob_l1.rho1);        std_error.l1_l1.rho2(iter1)    = std(e_rob_l1.rho2);
      min_error.l1_l1.rho1(iter1)    = min(e_rob_l1.rho1);        min_error.l1_l1.rho2(iter1)    = min(e_rob_l1.rho2);
      max_error.l1_l1.rho1(iter1)    = max(e_rob_l1.rho1);        max_error.l1_l1.rho2(iter1)    = max(e_rob_l1.rho2);
      p25_error.l1_l1.rho1(iter1)    = prctile(e_rob_l1.rho1,25); p25_error.l1_l1.rho2(iter1)    = prctile(e_rob_l1.rho2,25);
      p75_error.l1_l1.rho1(iter1)    = prctile(e_rob_l1.rho1,75); p75_error.l1_l1.rho2(iter1)    = prctile(e_rob_l1.rho2,75);
      
      median_error.l1_l2.rho1(iter1) = median(e_rob_l2.rho1);     median_error.l1_l2.rho2(iter1) = median(e_rob_l2.rho2);
      mean_error.l1_l2.rho1(iter1)   = mean(e_rob_l2.rho1);       mean_error.l1_l2.rho2(iter1)   = mean(e_rob_l2.rho2);
      std_error.l1_l2.rho1(iter1)    = std(e_rob_l2.rho1);        std_error.l1_l2.rho2(iter1)    = std(e_rob_l2.rho2);
      min_error.l1_l2.rho1(iter1)    = min(e_rob_l2.rho1);        min_error.l1_l2.rho2(iter1)    = min(e_rob_l2.rho2);
      max_error.l1_l2.rho1(iter1)    = max(e_rob_l2.rho1);        max_error.l1_l2.rho2(iter1)    = max(e_rob_l2.rho2);
      p25_error.l1_l2.rho1(iter1)    = prctile(e_rob_l2.rho1,25); p25_error.l1_l2.rho2(iter1)    = prctile(e_rob_l2.rho2,25);
      p75_error.l1_l2.rho1(iter1)    = prctile(e_rob_l2.rho1,75); p75_error.l1_l2.rho2(iter1)    = prctile(e_rob_l2.rho2,75);

      median_error.l1_linf.rho1(iter1) = median(e_rob_linf.rho1);     median_error.l1_linf.rho2(iter1) = median(e_rob_linf.rho2);
      mean_error.l1_linf.rho1(iter1)   = mean(e_rob_linf.rho1);       mean_error.l1_linf.rho2(iter1)   = mean(e_rob_linf.rho2);
      std_error.l1_linf.rho1(iter1)    = std(e_rob_linf.rho1);        std_error.l1_linf.rho2(iter1)    = std(e_rob_linf.rho2);
      min_error.l1_linf.rho1(iter1)    = min(e_rob_linf.rho1);        min_error.l1_linf.rho2(iter1)    = min(e_rob_linf.rho2);
      max_error.l1_linf.rho1(iter1)    = max(e_rob_linf.rho1);        max_error.l1_linf.rho2(iter1)    = max(e_rob_linf.rho2);
      p25_error.l1_linf.rho1(iter1)    = prctile(e_rob_linf.rho1,25); p25_error.l1_linf.rho2(iter1)    = prctile(e_rob_linf.rho2,25);
      p75_error.l1_linf.rho1(iter1)    = prctile(e_rob_linf.rho1,75); p75_error.l1_linf.rho2(iter1)    = prctile(e_rob_linf.rho2,75);

      median_error.nom_l1(iter1) = median(e_nom_l1);
      mean_error.nom_l1(iter1)   = mean(e_nom_l1);
      std_error.nom_l1(iter1)    = std(e_nom_l1);
      min_error.nom_l1(iter1)    = min(e_nom_l1);
      max_error.nom_l1(iter1)    = max(e_nom_l1);
      p25_error.nom_l1(iter1)    = prctile(e_nom_l1,25);
      p75_error.nom_l1(iter1)    = prctile(e_nom_l1,75);

      median_error.nom_l2(iter1) = median(e_nom_l2);
      mean_error.nom_l2(iter1)   = mean(e_nom_l2);
      std_error.nom_l2(iter1)    = std(e_nom_l2);
      min_error.nom_l2(iter1)    = min(e_nom_l2);
      max_error.nom_l2(iter1)    = max(e_nom_l2);
      p25_error.nom_l2(iter1)    = prctile(e_nom_l2,25);
      p75_error.nom_l2(iter1)    = prctile(e_nom_l2,75);

      median_error.nom_linf(iter1) = median(e_nom_linf);
      mean_error.nom_linf(iter1)   = mean(e_nom_linf);
      std_error.nom_linf(iter1)    = std(e_nom_linf);
      min_error.nom_linf(iter1)    = min(e_nom_linf);
      max_error.nom_linf(iter1)    = max(e_nom_linf);
      p25_error.nom_linf(iter1)    = prctile(e_nom_linf,25);
      p75_error.nom_linf(iter1)    = prctile(e_nom_linf,75);

  end %for

  tElapsed = toc;

  %% Save results

  save('IEEE_14_vary_sigma_results.mat');

end %function

function [xhat, Dhat] = robust_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda, rho)

    cvx_precision medium
    cvx_begin quiet

        variable xhat( n )
        variable Dhat( m , T)

        Phi     = [];
        Theta_D = [];

        for i = 1:T
            Phi     = [Phi C*A^(i-1)*xhat];
            sum_tmp = zeros(n,1);
            for j = 0:i-2
                tmp2 = i-2-j;
                if tmp2>=0
                    sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat(:,j+1));
                end %if
            end %for
            Theta_D=[Theta_D D*(u(:,i)+Dhat(:,i))+C*sum_tmp];
        end %for

        Ehat = Y-Phi-Theta_D;
        obj  = rho*norm(xhat,1);

        for i=1:p
            obj = obj+norm(Ehat(i,:),lr);
        end %for

        for i=1:m
            obj = obj+rho*norm(Dhat(i,:),1)+lambda*norm(Dhat(i,:),lr);
        end %for

        minimize( obj ) % Unconstrained minimization

    cvx_end            
    cvx_clear

end %function

function [xhat, Dhat] = nominal_estimator(A, B, C, D, u, Y, n, m, p, T, lr, lambda)

    cvx_precision medium
    cvx_begin quiet

        variable xhat( n )
        variable Dhat( m , T)

        Phi     = [];
        Theta_D = [];

        for i =1 :T
            Phi     = [Phi C*A^(i-1)*xhat];
            sum_tmp = zeros(n,1);
            for j = 0:i-2
                tmp2 = i-2-j;
                if tmp2 >= 0
                    sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat(:,j+1));
                end %if
            end %for
            Theta_D = [Theta_D D*(u(:,i)+Dhat(:,i))+C*sum_tmp];
        end %for

        Ehat = Y-Phi-Theta_D;
        obj = 0;

        for i = 1:p
            obj = obj+norm(Ehat(i,:),lr);
        end %for

        for i=1:m
            obj = obj+lambda*norm(Dhat(i,:),lr);
        end %for

        minimize( obj ) % Unconstrained minimization

    cvx_end            
    cvx_clear

end %function