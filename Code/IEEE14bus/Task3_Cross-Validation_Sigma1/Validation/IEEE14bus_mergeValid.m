% Project l1/linf (robust)
close all; clear all; clc;

%% Set up parameters

load IEEE14bus_DataSets.mat
% load mergedData.mat


lambdaNom=zeros(3,nSets); % 1: l1, 2: l2, 3: linf
lambdaRob=zeros(3,nSets);
rhoRob=zeros(3,nSets);

minNom=zeros(3,nSets);
minRob=zeros(3,nSets);

nValid=0.25*nData;

median_error_V = zeros(3,nSets);   % 1: l1, 2: l2, 3: linf
mean_error_V = zeros(3,nSets);
std_error_V = zeros(3,nSets);
max_error_V = zeros(3,nSets);
min_error_V = zeros(3,nSets);
p25_error_V = zeros(3,nSets);
p75_error_V = zeros(3,nSets);

median_error_rob_V = zeros(3,nSets);   % 1: l1, 2: l2, 3: linf
mean_error_rob_V = zeros(3,nSets);
std_error_rob_V = zeros(3,nSets);
max_error_rob_V = zeros(3,nSets);
min_error_rob_V = zeros(3,nSets);
p25_error_rob_V = zeros(3,nSets);
p75_error_rob_V = zeros(3,nSets);

lr_median_error_V = zeros(3,nSets);   % 1: l1, 2: l2, 3: linf; lambda; min
lr_mean_error_V = zeros(3,nSets);
lr_std_error_V = zeros(3,nSets);
lr_max_error_V = zeros(3,nSets);
lr_min_error_V = zeros(3,nSets);
lr_p25_error_V = zeros(3,nSets);
lr_p75_error_V = zeros(3,nSets);

lr_median_error_rob_V = zeros(4,nSets);   % 1: l1, 2: l2, 3: linf; lambda; rho; min
lr_mean_error_rob_V = zeros(4,nSets);
lr_std_error_rob_V = zeros(4,nSets);
lr_max_error_rob_V = zeros(4,nSets);
lr_min_error_rob_V = zeros(4,nSets);
lr_p25_error_rob_V = zeros(4,nSets);
lr_p75_error_rob_V = zeros(4,nSets);

tElapsed=0;

for ii=1:nSets
    name = {'l1_lr_Valid_nSet',num2str(ii),'.mat'};
    str = strjoin(name,'');
    if exist(str, 'file')
        tmp=load(str);

        tElapsed=tElapsed+tmp.tElapsed;
        
        lr_median_error_V = lr_median_error_V+tmp.lr_median_error_V;   
        lr_mean_error_V = lr_mean_error_V+tmp.lr_mean_error_V;   
        lr_std_error_V = lr_std_error_V+tmp.lr_std_error_V;   
        lr_max_error_V = lr_max_error_V+tmp.lr_max_error_V;   
        lr_min_error_V = lr_min_error_V+tmp.lr_min_error_V;   
        lr_p25_error_V = lr_p25_error_V+tmp.lr_p25_error_V;   
        lr_p75_error_V = lr_p75_error_V+tmp.lr_p75_error_V;   
        
        lr_median_error_rob_V = lr_median_error_rob_V+tmp.lr_median_error_rob_V;
        lr_mean_error_rob_V = lr_mean_error_rob_V+tmp.lr_mean_error_rob_V;
        lr_std_error_rob_V = lr_std_error_rob_V+tmp.lr_std_error_rob_V;
        lr_max_error_rob_V = lr_max_error_rob_V+tmp.lr_max_error_rob_V;
        lr_min_error_rob_V = lr_min_error_rob_V+tmp.lr_min_error_rob_V;
        lr_p25_error_rob_V = lr_p25_error_rob_V+tmp.lr_p25_error_rob_V;
        lr_p75_error_rob_V = lr_p75_error_rob_V+tmp.lr_p75_error_rob_V;
        
    end
end


str = 'l1_lr_Valid.mat';
% save('l1_lr_Valid.mat','tElapsed','lr_median_error_V','lr_mean_error_V','lr_median_error_rob_V','lr_mean_error_rob_V')
save(str,'tElapsed','lr_median_error_V','lr_mean_error_V','lr_std_error_V','lr_max_error_V','lr_min_error_V','lr_p25_error_V','lr_p75_error_V','lr_median_error_rob_V','lr_mean_error_rob_V','lr_std_error_rob_V','lr_max_error_rob_V','lr_min_error_rob_V','lr_p25_error_rob_V','lr_p75_error_rob_V')


display('Attack+Uncertainty')
