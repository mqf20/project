% Project

close all; clear all; clc;

%% Set up parameters

load IEEE14bus_Data.mat

nSets=20;
XMat_TrainSet=zeros(n,1,0.5*nData,nSets);
YMat_TrainSet=zeros(p,T,0.5*nData,nSets);
UMat_TrainSet=zeros(m,T,0.5*nData,nSets);

XMat_ValidSet=zeros(n,1,0.25*nData,nSets);
YMat_ValidSet=zeros(p,T,0.25*nData,nSets);
UMat_ValidSet=zeros(m,T,0.25*nData,nSets);

XMat_TestSet=zeros(n,1,0.25*nData,nSets);
YMat_TestSet=zeros(p,T,0.25*nData,nSets);
UMat_TestSet=zeros(m,T,0.25*nData,nSets);

for k=1:nSets
    
    rm=randperm(nData);
    XMat_TrainSet(:,:,:,k)=XMat(:,:,rm(1:0.5*nData));
    YMat_TrainSet(:,:,:,k)=YMat(:,:,rm(1:0.5*nData));
    UMat_TrainSet(:,:,:,k)=UMat(:,:,rm(1:0.5*nData));
    
    XMat_ValidSet(:,:,:,k)=XMat(:,:,rm(0.5*nData+1:0.75*nData));
    YMat_ValidSet(:,:,:,k)=YMat(:,:,rm(0.5*nData+1:0.75*nData));
    UMat_ValidSet(:,:,:,k)=UMat(:,:,rm(0.5*nData+1:0.75*nData));
    
    XMat_TestSet(:,:,:,k)=XMat(:,:,rm(0.75*nData+1:nData));
    YMat_TestSet(:,:,:,k)=YMat(:,:,rm(0.75*nData+1:nData));
    UMat_TestSet(:,:,:,k)=UMat(:,:,rm(0.75*nData+1:nData));

end

save('IEEE14bus_DataSets.mat','XMat','YMat','UMat','XMat_TrainSet','YMat_TrainSet','UMat_TrainSet','XMat_ValidSet','YMat_ValidSet','UMat_ValidSet','XMat_TestSet','YMat_TestSet','UMat_TestSet','n','m','p','q_a','q_s','T','A','B','C','D','nData','nSets')
% save('l1_lr_Array.mat','rho1','rho2','lambda','lr','median_error','median_error_r1','median_error_r2','mean_error','mean_error_r1','mean_error_r2','min_error','min_error_r1','min_error_r2','max_error','max_error_r1','max_error_r2','p25_error','p25_error_r1','p25_error_r2','p75_error','p75_error_r1','p75_error_r2')
display('Attack+Uncertainty')

