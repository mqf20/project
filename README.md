# 15.094 Robust Optimization Project

* Started working on project proposal
* Sze Zheng Yong szyong@mit.edu
* Ming Qing Foo mqf20@mit.edu

* Task 1: Normalized errors for an array of numbers of sensor and actuator attacks
* Task 2: Varying sigma
* Task 3: Cross validation
* Task 4: Forward propagation (trajectory)
* Task 5: Boxplot of error of initial state only

## Progress

### Task1_ActuatorSensorArray
* 5/10/15 17:30 : started running on building 38 Athena cluster (mrhalp - completed)

### Task2_varyingSigma
* 5/12/15 00:30 : (split) started running on building 38 Athena cluster (mrhalp - completed) (Part 2 incorrect)
* 5/12/15 13:30 : (split) started running on building 38 Athena cluster (mqf20 - 0472079) (Part 2a and 2b)

### Task3_Cross-Validation / Training / 
#### nset1 
##### rho = 1
* nSet1_l1_l1 (split) : 5/9/15 13:00 : started running on building 38 Athena cluster (mrhalp - completed)
* nSet1_l1_l2 : 5/7/15 17:00 : started running on building 38 Athena cluster (mrhalp - completed)
* nSet1_l1_linf (split) : 5/7/15 17:00 : started running on building 38 Athena cluster (mrhalp - completed)

##### rho = 2.5
* nSet1_l1_l1 : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0472129)
* nSet1_l1_l2 : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0472129)
* nSet1_l1_linf (split) : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - completed)

#### nset2
##### rho = 1
* nSet2_l1_l1 (split) : 5/11/15 15:00 : started running on building 38 Athena cluster (mrhalp - completed)
* nSet2_l1_l2 : 5/7/15 17:00 : started running on Sidpac Athena Cluster (mqf20 - completed)
* nSet2_l1_linf (split) : 5/7/15 17:00 : started running on Sidpac Athena Cluster (mqf20 - completed)

##### rho = 2.5
* nSet2_l1_l1 : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0471712) ???
* nSet2_l1_l2 : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0471712) ???
* nSet2_l1_linf (split) : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0471712) ???

#### nset3
##### rho = 1
* nSet3_l1_l1 (split) : 5/11/15 15:00 : started running on building 38 Athena cluster (mqf20 - completed)
* nSet3_l1_l2 : 5/8/15 14:00 : started running on Sidpac Athena Cluster (mrhalp - completed) 
* nSet3_l1_linf : 5/8/15 14:00 : started running on Sidpac Athena Cluster (mrhalp - completed)

##### rho = 2.5
* nSet3_l1_l1 : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0472141)
* nSet3_l1_l2 : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0472141)
* nSet3_l1_linf (split) : 5/12/15 17:30 : started running on building 38 Athena cluster (mrhalp - 0472141)

#### nset4
##### rho = 1
* nSet4_l1_l1 (split) : 5/9/15 13:00 : started running on building 38 Athena cluster (mqf20 - completed)
* nSet4_l1_l2 : 5/8/15 14:00 : started running on Sidpac Athena Cluster (mqf20 - completed)
* nSet4_l1_linf : 5/8/15 14:00 : started running on Sidpac Athena Cluster (mqf20 - completed)

##### rho = 2.5
* nSet4_l1_l1 : 5/12/15 17:30 : started running on building 38 Athena cluster (mqf20 - 0471701) ???
* nSet4_l1_l2 : 5/12/15 17:30 : started running on building 38 Athena cluster (mqf20 - 0471701) ???
* nSet4_l1_linf (split) : 5/12/15 17:30 : started running on building 38 Athena cluster (mqf20 - 0471701) ???

#### nset5
* nSet5_l1_l1 (split) : 5/11/15 15:30 : started running on building 38 Athena cluster (mrhalp - completed)
* nSet5_l1_l2 : 5/8/15 15:00 : started running on building 38 Athena cluster (mrhalp - completed)
* nSet5_l1_linf : 5/8/15 15:00 : started running on building 38 Athena cluster (mrhalp - completed)

#### nset6
* nSet6_l1_l1 (split) : 5/11/15 15:30 : started running on building 38 Athena cluster (mqf20 - completed)
* nSet6_l1_l2 : 5/8/15 15:00 : started running on building 38 Athena cluster (mqf20 - completed)
* nSet6_l1_linf : 5/8/15 15:00 : started running on building 38 Athena cluster (mqf20 - completed)

#### nset7
* nSet7_l1_l1 : 5/9/15 18:00 : started running on building 38 Athena cluster (mrhalp - completed)
* nSet7_l1_l2 : 5/9/15 18:00 : started running on building 38 Athena Cluster (mrhalp - completed)
* nSet7_l1_linf : 5/9/15 18:00 : started running on building 38 Athena Cluster (mrhalp - completed)

#### nset8
* nSet8_l1_l1 : 5/9/15 18:00 : started running on building 38 Athena cluster (mqf20 - completed)
* nSet8_l1_l2 : 5/9/15 18:00 : started running on building 38 Athena Cluster (mqf20 - completed)
* nSet8_l1_linf : 5/9/15 18:00 : started running on building 38 Athena Cluster (mqf20 - completed)

#### nset9
* nSet9_l1_l1 : 5/10/15 00:00 : started running on Sidpac Athena cluster (mrhalp - completed)
* nSet9_l1_l2 : 5/10/15 00:00 : started running on Sidpac Athena Cluster (mrhalp - completed)
* nSet9_l1_linf : 5/10/15 00:00 : started running on Sidpac Athena Cluster (mrhalp - completed)

#### nset10
* nSet10_l1_l1 : 5/10/15 00:00 : started running on Sidpac Athena cluster (mqf20 - completed)
* nSet10_l1_l2 : 5/10/15 00:00 : started running on Sidpac Athena Cluster (mqf20 - completed)
* nSet10_l1_linf : 5/10/15 00:00 : started running on Sidpac Athena Cluster (mqf20 - completed)

#### nset19
* nSet19_l1_l1 : (szyong - completed)
* nSet19_l1_l2 : (szyong - completed)
* nSet19_l1_linf : (szyong - completed)

#### nset20
* nSet20_l1_l1 : (szyong - completed)
* nSet20_l1_l2 : (szyong - completed)
* nSet20_l1_linf : (szyong - completed)