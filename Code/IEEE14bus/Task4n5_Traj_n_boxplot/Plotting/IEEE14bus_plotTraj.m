% Project l1/l2

clear all; clc; close all; 

% display('Uncertainty Only')
l2_both=load('l1_linf_traj_q13_both.mat');
l2_attack=load('l1_linf_traj_q13_attack.mat');
l2_error=load('l1_linf_traj_q13_error.mat');

T=size(l2_both.e_nom,2);
k=1:T;

mean_both_nom = mean(l2_both.e_nom);
std_both_nom = std(l2_both.e_nom);
mean_both_rob = mean(l2_both.e_rob);
std_both_rob = std(l2_both.e_rob);
mean_both_rob2 = mean(l2_both.e_rob2);
std_both_rob2 = std(l2_both.e_rob2);

mean_attack_nom = mean(l2_attack.e_nom);
std_attack_nom = std(l2_attack.e_nom);
mean_attack_rob = mean(l2_attack.e_rob);
std_attack_rob = std(l2_attack.e_rob);
mean_attack_rob2 = mean(l2_attack.e_rob2);
std_attack_rob2 = std(l2_attack.e_rob2);

mean_error_nom = mean(l2_error.e_nom);
std_error_nom = std(l2_error.e_nom);
mean_error_rob = mean(l2_error.e_rob);
std_error_rob = std(l2_error.e_rob);
mean_error_rob2 = mean(l2_error.e_rob2);
std_error_rob2 = std(l2_error.e_rob2);

median_error_nom = median(l2_error.e_nom);  
p25_error_nom = prctile(l2_error.e_nom,25);
p75_error_nom = prctile(l2_error.e_nom,75);
median_error_rob = median(l2_error.e_rob);  
p25_error_rob = prctile(l2_error.e_rob,25);
p75_error_rob = prctile(l2_error.e_rob,75);
median_error_rob2 = median(l2_error.e_rob2);  
p25_error_rob2 = prctile(l2_error.e_rob2,25);
p75_error_rob2 = prctile(l2_error.e_rob2,75);

median_attack_nom = median(l2_attack.e_nom);  
p25_attack_nom = prctile(l2_attack.e_nom,25);
p75_attack_nom = prctile(l2_attack.e_nom,75);
median_attack_rob = median(l2_attack.e_rob);  
p25_attack_rob = prctile(l2_attack.e_rob,25);
p75_attack_rob = prctile(l2_attack.e_rob,75);
median_attack_rob2 = median(l2_attack.e_rob2);  
p25_attack_rob2 = prctile(l2_attack.e_rob2,25);
p75_attack_rob2 = prctile(l2_attack.e_rob2,75);

median_both_nom = median(l2_both.e_nom);  
p25_both_nom = prctile(l2_both.e_nom,25);
p75_both_nom = prctile(l2_both.e_nom,75);
median_both_rob = median(l2_both.e_rob);  
p25_both_rob = prctile(l2_both.e_rob,25);
p75_both_rob = prctile(l2_both.e_rob,75);
median_both_rob2 = median(l2_both.e_rob2);  
p25_both_rob2 = prctile(l2_both.e_rob2,25);
p75_both_rob2 = prctile(l2_both.e_rob2,75);


% figure
% boxplot([e_rob(:,1),e_rob2(:,1),e_nom(:,1)],'whisker',1000)
% breakyaxis([p75_error_nom(1)+1 ceil(max_error_nom(1))-1]);
set(0,'defaulttextinterpreter','latex')
figure
subplot(3,1,1)%Normalized error as a function of time 
errorbar(k,mean_attack_nom,std_attack_nom,'k-')
hold all;
errorbar(k,mean_attack_rob,min(mean_attack_rob,std_attack_rob),std_attack_rob,'b--')
errorbar(k,mean_attack_rob2,std_attack_rob2,'r-.')
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold')
xlabel('Time step, $k$','FontSize',12,'fontWeight','bold') 
title('Attack Only','FontSize',12,'fontWeight','bold')
legend('nominal','robust $\rho=0.1$','robust $\rho=1.0$','Interpreter','latex','Orientation','horizontal')

subplot(3,1,2)
errorbar(k,mean_error_nom,std_error_nom,'k-')
hold all;
errorbar(k,mean_error_rob,std_error_rob,'b--')
errorbar(k,mean_error_rob2,std_error_rob2,'r-.')
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold')
xlabel('Time step, $k$','FontSize',12,'fontWeight','bold') 
title('Uncertainty Only','FontSize',12,'fontWeight','bold')
legend('nominal','robust $\rho=0.1$','robust $\rho=1.0$','Interpreter','latex','Orientation','horizontal')

subplot(3,1,3)
errorbar(k,mean_both_nom,std_both_nom,'k-')
hold all;
errorbar(k,mean_both_rob,std_both_rob,'b--')
errorbar(k,mean_both_rob2,std_both_rob2,'r-.')
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold')
xlabel('Time step, $k$','FontSize',12,'fontWeight','bold') 
title('Uncertainty and Attack','FontSize',12,'fontWeight','bold')
legend('nominal','robust $\rho=0.1$','robust $\rho=1.0$','Interpreter','latex','Orientation','horizontal')

text('Position',[2,2.7],'String','\textbf{Normalized mean error as a function of time step} $\mathbf{k}$','FontSize',12,'fontWeight','bold')


figure
subplot(3,1,1)%Normalized error as a function of time 
errorbar(k,median_attack_nom,median_attack_nom-p25_attack_nom,p75_attack_nom-median_attack_nom,'k-')
hold all;
errorbar(k,median_attack_rob,median_attack_rob-p25_attack_rob,p75_attack_rob-median_attack_rob,'b--')
errorbar(k,median_attack_rob2,median_attack_rob2-p25_attack_rob2,p75_attack_rob2-median_attack_rob2,'r-.')
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold')
xlabel('Time step, $k$','FontSize',12,'fontWeight','bold') 
title('Attack Only','FontSize',12,'fontWeight','bold')

subplot(3,1,2)
errorbar(k,median_error_nom,median_error_nom-p25_error_nom,p75_error_nom-median_error_nom,'k-')
hold all;
errorbar(k,median_error_rob,median_error_rob-p25_error_rob,p75_error_rob-median_error_rob,'b--')
errorbar(k,median_error_rob2,median_error_rob2-p25_error_rob2,p75_error_rob2-median_error_rob2,'r-.')
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold')
xlabel('Time step, $k$','FontSize',12,'fontWeight','bold') 
title('Uncertainty Only','FontSize',12,'fontWeight','bold')

subplot(3,1,3)
errorbar(k,median_both_nom,median_both_nom-p25_both_nom,p75_both_nom-median_both_nom,'k-')
hold all;
errorbar(k,median_both_rob,median_both_rob-p25_both_rob,p75_both_rob-median_both_rob,'b--')
errorbar(k,median_both_rob2,median_both_rob2-p25_both_rob2,p75_both_rob2-median_both_rob2,'r-.')
ylabel('$\mathbf{\|\hat{x}_0-x_0\|/\|x_0\|}$','FontSize',12,'fontWeight','bold')
xlabel('Time step, $k$','FontSize',12,'fontWeight','bold') 
title('Uncertainty and Attack','FontSize',12,'fontWeight','bold')

text('Position',[2,8.9],'String','\textbf{Normalized median error as a function of time step} $\mathbf{k}$','FontSize',12,'fontWeight','bold')

%%
% % median_error_nom = median(e_nom);   
% mean_error_nom = mean(e_nom);
% std_error_nom = std(e_nom);
% % max_error_nom = max(e_nom);
% % min_error_nom = min(e_nom);
% % p25_error_nom = prctile(e_nom,25);
% % p75_error_nom = prctile(e_nom,75);
% 
% % median_error_rob = median(e_rob);   
% mean_error_rob = mean(e_rob);
% std_error_rob = std(e_rob);
% % max_error_rob = max(e_rob);
% % min_error_rob = min(e_rob);
% % p25_error_rob = prctile(e_rob,25);
% % p75_error_rob = prctile(e_rob,75);
% 
% % median_error_rob2 = median(e_rob2);   
% mean_error_rob2 = mean(e_rob2);
% std_error_rob2 = std(e_rob2);
% % max_error_rob2 = max(e_rob2);
% % min_error_rob2 = min(e_rob2);
% % p25_error_rob2 = prctile(e_rob2,25);
% % p75_error_rob2 = prctile(e_rob2,75);
