% Project l1/l2

close all; clear all; clc;
Iter=500;
count=0;
avg=0;
avg_nom=0;

load Atilde.mat
load Ctilde.mat

Ts = 0.5;
A = expm(Atilde*Ts); n = size(A,1);
C = Ctilde; p = size(C,1);
B = [zeros(n/2,n/2); eye(n/2,n/2)];
m = size(B,2);
D = zeros(p,m);
% attackpower = 5;
% Nsim = 1; % # of simulations per value of q
% Tmax = 10;

rho1 = 0.025;
rho2 = 0.25;
lambda =1e-3;%0.15; % For l2, lambda=[5,15] (10), % For l_inf, lambda=[0.05,1] (0.5)
lr=inf; % 2 and inf, crashes occasionally when I use lr=1

% n = 20;
% m = 5;
% p = 20; %15
T = 1.5*n;%15; % Number of time-steps
q_s = 1;%5;
q_a = 3;%1;
% max_qa = 5;
% max_qs = 10;

e_rob=[];
e_rob2=[];
e_nom=[];

iter=1;

while iter <=  Iter %
% for iter=1:Iter
    
    

x0=randn(n,1);

rm=randperm(m);
ra=rm(1:m-q_a);
G=eye(m);
G(:,ra)=[];

rp=randperm(p-1);
rs=rp(1:p-1-q_s);
H=eye(p);
H(:,rs)=[];
H(:,end)=[]; % the last measurement (the rotor angle) cannot be attacked


% A = 0.1.*randn(n,n); B = randn(n,m);
% C = randn(p,n); D = randn(p,m);
dA = [zeros(n/2,n); 0.01*randn(n/2,n/2), zeros(n/2,n/2)]; 
dB = [zeros(n/2,m);diag(0.1*randn(m,1))];
dC = [0.1*randn(p,n/2), zeros(p,n/2)]; dD = 0;%0.1*randn(p,m);
A_tilde = expm((Atilde+dA)*Ts);
B_tilde =B+dB;
C_tilde=C+dC; D_tilde =D+dD;

display(iter)
if ~(all(abs(eig(A))<=1+1e-10) && all(abs(eig(A_tilde))<1+1e-10))
%     iter=iter+1;
%     display(iter)
%     display('accept')
% else
    display('reject')
    continue
else
    iter=iter+1;
end

Q=[eps*eye(n/2),zeros(n/2,n/2);zeros(n/2,n/2), 1e-1*eye(n/2)];%diag([0.1 0.16 0.2 0.12 0.25 0.14 0.03 0.21 0.3 0.02 0.09 0.18]);
R=1e-1*eye(p);%*diag([0.21 0.06 0.22 0.02 0.19 0.14 0.13 0.11 0.23 0.12 0.03 0.18]);

% T time steps
k=1:T;
attackpower = 1;
d=attackpower*randn(q_a,length(k));
e=attackpower*randn(q_s,length(k));
u=0.01*randn(m,length(k));
% u=ones(m,length(k));
% d=ones(q_a,length(k));
% for i=1:q_a
%     d(i,:)=i*randn.*d(i,:);
%     u(i,:)=sqrt(i)*randn.*u(i,:);
% end
% e=ones(q_s,length(k));
% for i=1:q_s
%     e(i,:)=i^2*randn.*e(i,:);
% end


% Generate w and v
SigmaQ = chol(Q);
w = (randn(length(k),n)*SigmaQ)';
SigmaR = chol(R);
v = (randn(length(k),p)*SigmaR)';

% Dynamics
x=zeros(n,length(k));
y=zeros(p,length(k));
x(:,1)=x0;
for i=k(1:k(end))
%     y(:,i)=C_tilde*x(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
%     if i<k(end)
%         x(:,i+1)=A_tilde*x(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
%     end
    % no uncertainties
    y(:,i)=C*x(:,i)+D*(u(:,i)+G*d(:,i))+H*e(:,i);
    if i<k(end)
        x(:,i+1)=A*x(:,i)+B*(u(:,i)+G*d(:,i));
    end
end
Y=y;

cvx_solver Gurobi
cvx_precision medium
cvx_begin quiet
variable xhat( n ) 
variable Dhat( m , T)
Phi=[];
Theta=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhat];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat(:,j+1)); 
       end
    end
    Theta=[Theta D*(u(:,i)+Dhat(:,i))+C*sum_tmp];
end
Ehat=Y-Phi-Theta;
obj=rho1*norm(xhat,1);
for i=1:p
    obj=obj+norm(Ehat(i,:),lr);
end
for i=1:m
    obj=obj+rho1*norm(Dhat(i,:),1)+lambda*norm(Dhat(i,:),lr);
end
% obj
% sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2))
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2));
minimize( obj )

cvx_end

cvx_clear

cvx_solver Gurobi
cvx_precision medium
cvx_begin quiet
variable xhat2( n ) 
variable Dhat2( m , T)
Phi=[];
Theta=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhat2];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhat2(:,j+1)); 
       end
    end
    Theta=[Theta D*(u(:,i)+Dhat2(:,i))+C*sum_tmp];
end
Ehat2=Y-Phi-Theta;
obj=rho2*norm(xhat2,1);
for i=1:p
    obj=obj+norm(Ehat2(i,:),lr);
end
for i=1:m
    obj=obj+rho2*norm(Dhat2(i,:),1)+lambda*norm(Dhat2(i,:),lr);
end
% obj
% sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2))
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhat,lr,2));
minimize( obj )

cvx_end

cvx_clear

% Nominal
%rho=0;
cvx_solver Gurobi
cvx_precision medium
cvx_begin quiet
variable xhatnom( n ) 
variable Dhatnom( m , T)
Phi=[];
Theta=[];
for i=1:T
    Phi=[Phi C*A^(i-1)*xhatnom];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1)+Dhatnom(:,j+1)); 
       end
    end
    Theta=[Theta D*(u(:,i)+Dhatnom(:,i))+C*sum_tmp];
end
Ehat=Y-Phi-Theta;
obj=0;%rho*norm(xhatnom,1);
for i=1:p
    obj=obj+norm(Ehat(i,:),lr);
end
for i=1:m
    obj=obj+lambda*norm(Dhatnom(i,:),lr);%+rho*norm(Dhatnom(i,:),1);
end
% obj=sum(norms(Ehat,lr,2)) + lambda*sum(norms(Dhatnom,lr,2));
minimize( obj )

cvx_end

cvx_clear


% O=[];
% for j=0:T-1
%    O=[O;C*A^j]; 
% end
% O_tilde=[];
% for j=0:T-1
%    O_tilde=[O_tilde;C_tilde*A_tilde^j]; 
% end
% dO=O_tilde-O;
% J=zeros(p*T,m*T);
% for i=1:T
%     for j=1:i
%         if i==j
%             J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D;
%         else
%             for k=1:T-1
%                 if i-j==k
%                     J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C*A^(k-1)*B;
%                 end
%             end
%         end
%     end
% end
% J_tilde=zeros(p*T,m*T);
% for i=1:T
%     for j=1:i
%         if i==j
%             J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=D_tilde;
%         else
%             for k=1:T-1
%                 if i-j==k
%                     J(1+(i-1)*p:p+(i-1)*p,1+(j-1)*m:m+(j-1)*m)=C_tilde*A_tilde^(k-1)*B_tilde;
%                 end
%             end
%         end
%     end
% end
% dJ=J_tilde-J;
% Jw=zeros(p*T,n*T);
% for i=1:T
%     for j=1:i        
%         for k=1:T-1
%             if i-j==k
%                 Jw(1+(i-1)*p:p+(i-1)*p,1+(j-1)*n:n+(j-1)*n)=C*A^(k-1);
%             end
%         end
%     end
% end
% noise=Jw*vec(w)+vec(v);
% dPsi=[dO J_tilde noise];
% 
% norm1=norm(dPsi,1);
% normInf=norm(dPsi,inf);
% error=norm((xhat-x0));
% error_nom=norm((xhatnom-x0));
error=norm((xhat-x0))/(norm(x0)+eps);%mean(abs(xhat-x0)./abs(x0+eps));%norm((xhat-x0)./(x0+eps))/sqrt(n);%norm((xhat-x0)./(x0+eps));%
error2=norm((xhat2-x0))/(norm(x0)+eps);
error_nom=norm((xhatnom-x0))/(norm(x0)+eps);%mean(abs(xhatnom-x0)./abs(x0+eps));%norm((xhatnom-x0)./(x0+eps))/sqrt(n);%
if error_nom-error<0
    count=count+1;
end
avg=avg+error;
avg_nom=avg_nom+error_nom;

% figure(1)
% hold all
% scatter(norm1,error,'bx')
% scatter(norm1,error_nom,'ro')
% figure(2)
% hold all
% scatter(normInf,error,'bx')
% scatter(normInf,error_nom,'ro')
e_rob=[e_rob; error];
e_rob2=[e_rob2; error2];
e_nom=[e_nom; error_nom];


end
% avg=avg/Iter
% avg_nom=avg_nom/Iter
boxplot([e_rob,e_rob2,e_nom],'whisker',1000)
display(count)

%  save('l1_linf_rho_25_lambda0_001_q13_attack_p2.mat','e_rob','e_rob2','e_nom','count')
display('Attack Only')
% rho
% for i = 1 : size(r,2)
%   % Parse r
%   optval(i) = r{i}.optval;
%   Gamma(i) = r{i}.Gamma;
%   % Generate violation probabilities (from Lecture 5 page 10)
%   Phi(i) = qfunc((Gamma(i)-1)/sqrt(F*C));
%   
% end  %for
% 
% 
% % figure; 
% % plot(Gamma,optval,'*'); grid on;
% % xlabel('Gamma'); ylabel('Worst-case cost');
% 
% figure;
% plot(Gamma,Phi)
% [ax, p1, p2] = plotyy(Gamma,optval,Gamma,Phi); grid on;
% p1.LineStyle = '--';
% p1.LineWidth = 2;
% p2.LineWidth = 2;
% ylabel(ax(1),'Worst-case cost') % label left y-axis
% ylabel(ax(2),'Violation probability') % label right y-axis
% xlabel(ax(1),'\Gamma') % label x-axis
% 
