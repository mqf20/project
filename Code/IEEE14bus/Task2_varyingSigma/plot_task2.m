function plot_task2()

    % This simple function plots the results of Task 2
    
    load('IEEE_14_vary_sigma_results.mat');

    figure;

    subplot(2,3,1); hold on;
    plot(sigmaVec, median_error.l1_l1.rho1, 'b-');
    plot(sigmaVec, median_error.l1_l1.rho2, 'r:');
    plot(sigmaVec, median_error.nom_l1, 'm-.');
    title('l1/l1 Robust Estimator');
    xlabel('\sigma'); ylabel('Median relative error');
    str1 = sprintf('Robust l1/l1 estimator with \\rho = %.1f', rho1);
    str2 = sprintf('Robust l1/l1 estimator with \\rho = %.1f', rho2);
    legend(str1, str2, 'Nominal l1 estimator', 'Location', 'NorthWest');

    subplot(2,3,2); hold on;
    plot(sigmaVec, median_error.l1_l2.rho1, 'b-');
    plot(sigmaVec, median_error.l1_l2.rho2, 'r:');
    plot(sigmaVec, median_error.nom_l2, 'm-.');
    title('l1/l2 Robust Estimator');
    xlabel('\sigma'); ylabel('Median relative error');
    str1 = sprintf('Robust l1/l2 estimator with \\rho = %.1f', rho1);
    str2 = sprintf('Robust l1/l2 estimator with \\rho = %.1f', rho2);
    legend(str1, str2, 'Nominal l2 estimator', 'Location', 'NorthWest');

    subplot(2,3,3); hold on;
    plot(sigmaVec, median_error.l1_linf.rho1, 'b-');
    plot(sigmaVec, median_error.l1_linf.rho2, 'r:');
    plot(sigmaVec, median_error.nom_linf, 'm-.');
    title('l1/linf Robust Estimator');
    xlabel('\sigma'); ylabel('Median relative error');
    str1 = sprintf('Robust l1/linf estimator with \\rho = %.1f', rho1);
    str2 = sprintf('Robust l1/linf estimator with \\rho = %.1f', rho2);
    legend(str1, str2, 'Nominal linf estimator', 'Location', 'NorthWest');

    subplot(2,3,4); hold on;
    plot(sigmaVec, mean_error.l1_l1.rho1, 'b-');
    plot(sigmaVec, mean_error.l1_l1.rho2, 'r:');
    plot(sigmaVec, mean_error.nom_l1, 'm-.');
    title('l1/l1 Robust Estimator');
    xlabel('\sigma'); ylabel('Mean relative error');
    str1 = sprintf('Robust l1/l1 estimator with \\rho = %.1f', rho1);
    str2 = sprintf('Robust l1/l1 estimator with \\rho = %.1f', rho2);
    legend(str1, str2, 'Nominal l1 estimator', 'Location', 'NorthWest');

    subplot(2,3,5); hold on;
    plot(sigmaVec, mean_error.l1_l2.rho1, 'b-');
    plot(sigmaVec, mean_error.l1_l2.rho2, 'r:');
    plot(sigmaVec, mean_error.nom_l2, 'm-.');
    title('l1/l2 Robust Estimator');
    xlabel('\sigma'); ylabel('Mean relative error');
    str1 = sprintf('Robust l1/l2 estimator with \\rho = %.1f', rho1);
    str2 = sprintf('Robust l1/l2 estimator with \\rho = %.1f', rho2);
    legend(str1, str2, 'Nominal l2 estimator', 'Location', 'NorthWest');

    subplot(2,3,6); hold on;
    plot(sigmaVec, mean_error.l1_linf.rho1, 'b-');
    plot(sigmaVec, mean_error.l1_linf.rho2, 'r:');
    plot(sigmaVec, mean_error.nom_linf, 'm-.');
    title('l1/linf Robust Estimator');
    xlabel('\sigma'); ylabel('Mean relative error');
    str1 = sprintf('Robust l1/linf estimator with \\rho = %.1f', rho1);
    str2 = sprintf('Robust l1/linf estimator with \\rho = %.1f', rho2);
    legend(str1, str2, 'Nominal linf estimator', 'Location', 'NorthWest');
    
end %function
