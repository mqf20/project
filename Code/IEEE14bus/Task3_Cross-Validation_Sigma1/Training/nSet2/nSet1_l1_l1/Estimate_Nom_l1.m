
function xhatnom=Estimate_Nom_l1(Y,u,O,J,n,m,p,T,lambda)

Yvec=vec(Y);

% Nominal estimator
%  Same as the robust estimator but with rho=0;

cvx_solver sdpt3 %Gurobi
cvx_precision medium
cvx_begin quiet

variable xhatnom( n )
variable dhatnom( m*T)

ehat=Yvec-O*xhatnom-J*(vec(u)+vec(dhatnom));
obj=norm(ehat,1)+lambda*norm(dhatnom,1);
minimize( obj ) % Unconstrained minimization

cvx_end
cvx_clear

end