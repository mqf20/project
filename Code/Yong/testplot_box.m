% l1/l2 relation
% data = rand(20,12);
% month = repmat({'No attack or uncertainty' 'Attack only' 'Uncertainty only' 'Both (lambda=1,rho=1)' 'Both (lambda=1,rho=1)' 'Both (lambda=1,rho=1)'},1,2);
% simobs = [repmat({'sim'},1,6),repmat({'obs'},1,6)];
% boxplot(data,{month,simobs},'colors',repmat('rb',1,6),'factorgap',[5 2],'labelverbosity','minor');
close all ; 
clear all;
clc 
% data = rand(100,12);
% data = rand(100,12);
l2_1=load('l1_l2_rho0_5_lambda0_01_new.mat');
l2_2=load('l1_l2_rho5_lambda0_01_new.mat');
l2_attack1=load('l1_l2_rho0_5_lambda0_01_attack_new.mat');
l2_attack2=load('l1_l2_rho5_lambda0_01_attack_new.mat');
l2_error1=load('l1_l2_rho0_5_lambda0_01_error_new.mat');
l2_error2=load('l1_l2_rho5_lambda0_01_error_new.mat');
% data = textread('bp.txt');  % Read user?s data file   
%{Assign the observed temperature to be Month_O and the simulated 
% temperature to be Month_S}% 
Jan_O = l2_attack1.e_nom;%data(:, 1); 
Jan_S = l2_attack1.e_rob;%data(:, 2); 
Feb_O = l2_attack2.e_nom;%data(:, 3); 
Feb_S = l2_attack2.e_rob;%data(:, 4); 
Mar_O = l2_error1.e_nom;%data(:, 5); 
Mar_S = l2_error1.e_rob;%data(:, 6); 
Apr_O = l2_error2.e_nom;%data(:, 7); 
Apr_S = l2_error2.e_rob;%data(:, 8); 
May_O = l2_1.e_nom;%data(:, 9); 
May_S = l2_1.e_rob;%data(:,10); 
Jun_O = l2_2.e_nom;%data(:,11); 
Jun_S = l2_2.e_rob;%data(:,12);
% Jul_O = data(:,13); 
% Jul_S = data(:,14); 
% Aug_O = data(:,15); 
% Aug_S = data(:,16); 
% Sep_O = data(:,17); 
% Sep_S = data(:,18); 
% Oct_O = data(:,19); 
% Oct_S = data(:,20); 
% Nov_O = data(:,21); 
% Nov_S = data(:,22); 
% Dec_O = data(:,23); 
% Dec_S = data(:,24);   
f=figure;   
% Boxplot for the observed temperature from January to December 
Temp_O = [Jan_O, Feb_O, Mar_O, Apr_O, May_O, Jun_O];%, Jul_O, Aug_O, Sep_O, Oct_O, Nov_O, Dec_O]; 
position_O = 1:1:6;  
% Define position for 12 Month_O boxplots  
box_O = boxplot(Temp_O,'colors','b','positions',position_O,'width',0.18,'whisker',1000);
set(gca,'XTickLabel',{' '})  % Erase xlabels   
hold on  % Keep the Month_O boxplots on figure overlap the Month_S boxplots   
% Boxplot for the simulated temperature from January to December 
Temp_S = [Jan_S, Feb_S, Mar_S, Apr_S, May_S, Jun_S];%, Jul_S, Aug_S, Sep_S, Oct_S, Nov_S, Dec_S]; 
position_S = 1.3:1:6.3;  % Define position for 12 Month_S boxplots  
box_S = boxplot(Temp_S,'colors','r','positions',position_S,'width',0.18,'whisker',1000);   
hold off   % Insert texts and labels 
ylabel('||xhat-x0||/||x0||') 
text('Position',[0.85,-0.25],'String','Nom') 
text('Position',[1.2,-0.25],'String','Rob') 
text('Position',[1.85,-0.25],'String','Nom') 
text('Position',[2.2,-0.25],'String','Rob') 
text('Position',[2.85,-0.25],'String','Nom') 
text('Position',[3.2,-0.25],'String','Rob') 
text('Position',[3.85,-0.25],'String','Nom') 
text('Position',[4.2,-0.25],'String','Rob') 
text('Position',[4.85,-0.25],'String','Nom') 
text('Position',[5.2,-0.25],'String','Rob') 
text('Position',[5.85,-0.25],'String','Nom')
text('Position',[6.2,-0.25],'String','Rob')

% text('Position',[0.85,-3],'String','Perfect sys') 
text('Position',[0.8,-0.4],'String','Attack only') 
text('Position',[0.8,-0.55],'String','\lambda=0.01,\rho=0.5')
text('Position',[1.8,-0.4],'String','Attack only') 
text('Position',[1.8,-0.55],'String','\lambda=0.01,\rho=5')
text('Position',[2.65,-0.4],'String','Uncertainty only') 
text('Position',[2.65,-0.55],'String','\lambda=0.01,\rho=0.5')
text('Position',[3.7,-0.4],'String','Uncertainty only') 
text('Position',[3.7,-0.55],'String','\lambda=0.01,\rho=5') 
text('Position',[4.8,-0.4],'String','\lambda=0.01,\rho=0.5') 
text('Position',[5.8,-0.4],'String','\lambda=0.01,\rho=5') 
xlim([0.6 6.7])
ylim([-0.1 5])
% text('Position',[7.1,0],'String','July') 
% text('Position',[8.1,0],'String','August') 
% text('Position',[9.1,0],'String','September') 
% text('Position',[10.1,0],'String','October') 
% text('Position',[11.1,0],'String','November') 
% text('Position',[12.1,0],'String','December') 
set(gca,'XTickLabel',{''});   % To hide outliers 
out_O = box_O(end,~isnan(box_O(end,:)));  
delete(out_O)  
out_S = box_S(end,~isnan(box_S(end,:)));  
delete(out_S)
title('l_1/l_2 Relaxation')