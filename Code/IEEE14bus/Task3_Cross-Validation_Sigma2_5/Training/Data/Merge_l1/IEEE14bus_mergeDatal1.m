% Project l1/l1 (robust)
close all; clear all; clc;

%% Set up parameters

load ../../IEEE14bus_DataSets.mat

nSet=5;

lambdaVec =  logspace(-4,2,20);
rhoVec = 0.01:0.025:0.025*20;
lr=1;%inf; % 2 and inf, crashes occasionally when I use lr=1

nTrain=0.5*nData;

nRho=length(rhoVec);
nLambda=length(lambdaVec);
rLambda=length(lambdaVec)/2+1:length(lambdaVec);

median_error = zeros(nLambda,1,nSets);   % (qactuators,qsensors)
mean_error = zeros(nLambda,1,nSets);
max_error = zeros(nLambda,1,nSets);
min_error = zeros(nLambda,1,nSets);
p25_error = zeros(nLambda,1,nSets);
p75_error = zeros(nLambda,1,nSets);

median_error_rob = zeros(nLambda,nRho,nSets);   % (qactuators,qsensors)
mean_error_rob = zeros(nLambda,nRho,nSets);
max_error_rob = zeros(nLambda,nRho,nSets);
min_error_rob = zeros(nLambda,nRho,nSets);
p25_error_rob = zeros(nLambda,nRho,nSets);
p75_error_rob = zeros(nLambda,nRho,nSets);

name = {'part1_l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
part1=load(str);
name = {'part2_l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
part2=load(str);
name = {'part3_l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
part3=load(str);
name = {'part4_l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
part4=load(str);

median_error = part1.median_error+part2.median_error+part3.median_error+part4.median_error;
mean_error = part1.mean_error+part2.mean_error+part3.mean_error+part4.mean_error;
max_error = part1.max_error+part2.max_error+part3.max_error+part4.max_error;
min_error = part1.min_error+part2.min_error+part3.min_error+part4.min_error;
p25_error = part1.p25_error+part2.p25_error+part3.p25_error+part4.p25_error;
p75_error = part1.p75_error+part2.p75_error+part3.p75_error+part4.p75_error;

median_error_rob = part1.median_error_rob+part2.median_error_rob+part3.median_error_rob+part4.median_error_rob;
mean_error_rob = part1.mean_error_rob+part2.mean_error_rob+part3.mean_error_rob+part4.mean_error_rob;
max_error_rob = part1.max_error_rob+part2.max_error_rob+part3.max_error_rob+part4.max_error_rob;
min_error_rob = part1.min_error_rob+part2.min_error_rob+part3.min_error_rob+part4.min_error_rob;
p25_error_rob = part1.p25_error_rob+part2.p25_error_rob+part3.p25_error_rob+part4.p25_error_rob;
p75_error_rob = part1.p75_error_rob+part2.p75_error_rob+part3.p75_error_rob+part4.p75_error_rob;
tElapsed = part1.tElapsed+part2.tElapsed;

% m=min(A(A>0)); 

name = {'l1_l',num2str(lr),'_Train_nSet',num2str(nSet),'.mat'};
str = strjoin(name,'');
save(str,'tElapsed','rhoVec','lambdaVec','lr','median_error','median_error_rob','mean_error','mean_error_rob','min_error','min_error_rob','max_error','max_error_rob','p25_error','p25_error_rob','p75_error','p75_error_rob')
display('Attack+Uncertainty')
