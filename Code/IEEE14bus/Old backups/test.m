% Project l1/l2

close all; clear all; clc;
% Iter=5;
% count=0;
% avg=0;
% avg_nom=0;

load Atilde.mat
load Ctilde.mat

Ts = 0.5;
A = expm(Atilde*Ts); n = size(A,1);
C = Ctilde; p = size(C,1);
B = [zeros(n/2,n/2); eye(n/2,n/2)];
m = size(B,2);
D = zeros(p,m);
% attackpower = 5;
% Nsim = 1; % # of simulations per value of q
% Tmax = 10;

rho1 = 0.025;
rho2 = 0.25;
lambda =1e-3;%0.15; % For l2, lambda=[5,15] (10), % For l_inf, lambda=[0.05,1] (0.5)
lr=inf; % 2 and inf, crashes occasionally when I use lr=1

% n = 20;
% m = 5;
% p = 20; %15
T = 1.5*n;%15; % Number of time-steps
q_s = 1;%5;
q_a = 3;%1;
% max_qa = 5;
% max_qs = 10;

e_rob=[];
e_rob2=[];
e_nom=[];

% iter=1;
% 
% while iter <=  Iter %
% for iter=1:Iter
    
    

x0=randn(n,1);

rm=randperm(m);
ra=rm(1:m-q_a);
G=eye(m);
G(:,ra)=[];

rp=randperm(p);
rs=rp(1:p-q_s);
H=eye(p);
H(:,rs)=[];


% A = 0.1.*randn(n,n); B = randn(n,m);
% C = randn(p,n); D = randn(p,m);
% dA = [zeros(n/2,n); 0.01*randn(n/2,n/2), zeros(n/2,n/2)]; 
% dB = [zeros(n/2,m);diag(0.1*randn(m,1))];
% dC = [0.1*randn(p,n/2), zeros(p,n/2)]; dD = 0;%0.1*randn(p,m);
% A_tilde = expm((Atilde+dA)*Ts);
% B_tilde =B+dB;
% C_tilde=C+dC; D_tilde =D+dD;
% 
% display(iter)
% if ~(all(abs(eig(A))<=1+1e-10) && all(abs(eig(A_tilde))<1+1e-10))
% %     iter=iter+1;
% %     display(iter)
% %     display('accept')
% % else
%     display('reject')
%     continue
% else
%     iter=iter+1;
% end

% Q=[eps*eye(n/2),zeros(n/2,n/2);zeros(n/2,n/2), 1e-1*eye(n/2)];%diag([0.1 0.16 0.2 0.12 0.25 0.14 0.03 0.21 0.3 0.02 0.09 0.18]);
% R=1e-1*eye(p);%*diag([0.21 0.06 0.22 0.02 0.19 0.14 0.13 0.11 0.23 0.12 0.03 0.18]);

% T time steps
k=1:T;
% u=ones(m,length(k));
% d=ones(q_a,length(k));
attackpower = 5;
d=attackpower*randn(q_a,length(k));
e=attackpower*randn(q_s,length(k));
u=randn(m,length(k));
% d(K) = attackpower*randn(length(K),1);
% for i=1:q_a
%     d(i,:)=i*randn.*d(i,:);
%     u(i,:)=sqrt(i)*randn.*u(i,:);
% end
% e=ones(q_s,length(k));
% for i=1:q_s
%     e(i,:)=i^2*randn.*e(i,:);
% end


% % Generate w and v
% SigmaQ = chol(Q);
% w = (randn(length(k),n)*SigmaQ)';
% SigmaR = chol(R);
% v = (randn(length(k),p)*SigmaR)';

% Dynamics
x=zeros(n,length(k));
x2=zeros(n,length(k));
y=zeros(p,length(k));
y2=zeros(p,length(k));
x(:,1)=x0;
for i=k(1:k(end))
%     y(:,i)=C_tilde*x(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
%     if i<k(end)
%         x(:,i+1)=A_tilde*x(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
%     end
    % no uncertainties
    y(:,i)=C*x(:,i)+D*(u(:,i)+G*d(:,i))+H*e(:,i);
    if i<k(end)
        x(:,i+1)=A*x(:,i)+B*(u(:,i)+G*d(:,i));
    end
end
Y=y;

x2(:,1)=x0;
for i=k(1:k(end))
%     y(:,i)=C_tilde*x(:,i)+D_tilde*(u(:,i)+G*d(:,i))+H*e(:,i)+v(:,i);
%     if i<k(end)
%         x(:,i+1)=A_tilde*x(:,i)+B_tilde*(u(:,i)+G*d(:,i))+w(:,i);
%     end
    % no uncertainties
    y2(:,i)=C*x2(:,i)+D*(G*d(:,i))+H*e(:,i);
    if i<k(end)
        x2(:,i+1)=A*x2(:,i)+B*(G*d(:,i));
    end
end
Y2=y2;

% cvx_solver Gurobi
% cvx_precision medium
% cvx_begin quiet
% variable xhat( n ) 
% variable Dhat( m , T)
Phi=[];
Theta=[];
for i=1:T
%     Phi=[Phi C*A^(i-1)*xhat];
    sum_tmp=zeros(n,1);
    for j=0:i-2
       tmp2=i-2-j;
       if tmp2>=0
            sum_tmp=sum_tmp+A^(tmp2)*B*(u(:,j+1));%+Dhat(:,j+1)); 
       end
    end
    Theta=[Theta C*sum_tmp+D*(u(:,i))];%+Dhat(:,i))];
end
display(Y-Theta-Y2)
% display(Y2)
